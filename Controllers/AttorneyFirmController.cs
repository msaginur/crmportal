﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Oasis.CRMIntegration;

namespace CRMPortal
{
    public class AttorneyFirmController : BaseController
    {

        [HttpPost]
        [ActionName("AddFirmToCTX")]
        public HttpResponseMessage AddFirmToCTX([FromBody]object value)
        {
            object result = null;
            int firmId;

            try
            {
                Firm firm = JsonConvert.DeserializeObject<Firm>(value.ToString());

                CRMPortalRepository rep = new CRMPortalRepository();

                firmId = DataRepository.AddFirm(firm, CRMPortalUserID);

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), "OK", RequestDuration);
                result = new { Status = "Success", CTX_FirmID = firmId };

            }
            catch (Exception ex)
            {
                string message = ex.Message + Environment.NewLine + ex.StackTrace;

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), message, RequestDuration);
                result = new { Status = "Fail" };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);

        }

        [HttpPost]
        [ActionName("AddFirmLocationToCTX")]
        public HttpResponseMessage AddFirmLocationToCTX([FromBody]object value)
        {
            object result = null;
            int firmILocationId;

            try
            {
                FirmLocation firmLocation = JsonConvert.DeserializeObject<FirmLocation>(value.ToString());

                CRMPortalRepository rep = new CRMPortalRepository();

                firmILocationId = DataRepository.AddFirmLocation(firmLocation, CRMPortalUserID);

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), "OK", RequestDuration);
                result = new { Status = "Success", CTX_FirmLocationID = firmILocationId };

            }
            catch (Exception ex)
            {
                string message = ex.Message + Environment.NewLine + ex.StackTrace;

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), message, RequestDuration);
                result = new { Status = "Fail" };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);

        }

        [HttpPost]
        [ActionName("AddAttorneyToCTX")]
        public HttpResponseMessage AddAttorneyToCTX([FromBody]object value)
        {
            object result = null;

            try
            {
                AttorneyInfo attorney = JsonConvert.DeserializeObject<AttorneyInfo>(value.ToString());

                CRMPortalRepository rep = new CRMPortalRepository();

                int attorneyId = DataRepository.AddAttorney(attorney, CRMPortalUserID);

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), "OK", RequestDuration);
                result = new { Status = "Success", CTX_AttorneyID = attorneyId };

            }
            catch (Exception ex)
            {
                string message = ex.Message + Environment.NewLine + ex.StackTrace;

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), message, RequestDuration);
                result = new { Status = "Fail" };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);

        }

        [HttpPost]
        [ActionName("AddAttorneyAssistantToCTX")]
        public HttpResponseMessage AddAttorneyAssistantToCTX([FromBody]object value)
        {
            object result = null;

            try
            {
                AttorneyAssistant attorneyAssistant = JsonConvert.DeserializeObject<AttorneyAssistant>(value.ToString());

                CRMPortalRepository rep = new CRMPortalRepository();

                int attorenyAssistantId = DataRepository.AddAttorneyAssistant(attorneyAssistant, CRMPortalUserID);

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), "OK", RequestDuration);
                result = new { Status = "Success", CTX_AttorneyAssistantID = attorenyAssistantId };

            }
            catch (Exception ex)
            {
                string message = ex.Message + Environment.NewLine + ex.StackTrace;

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), message, RequestDuration);
                result = new { Status = "Fail" };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);

        }

        [HttpPost]
        [ActionName("UpdateFirmToCTX")]
        public HttpResponseMessage UpdateFirmToCTX([FromBody]object value)
        {
            object result = null;

            try
            {
                Firm firm = JsonConvert.DeserializeObject<Firm>(value.ToString());

                DataRepository.UpdateFirm(firm, CRMPortalUserID);

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), "OK", RequestDuration);
                result = new { Status = "Success" };

            }
            catch (Exception ex)
            {
                string message = ex.Message + Environment.NewLine + ex.StackTrace;

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), message, RequestDuration);
                result = new { Status = "Fail" };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);

        }

        [HttpPost]
        [ActionName("UpdateFirmLocationToCTX")]
        public HttpResponseMessage UpdateFirmLocationToCTX([FromBody]object value)
        {
            object result = null;
            int firmILocationId;

            try
            {
                FirmLocation firmLocation = JsonConvert.DeserializeObject<FirmLocation>(value.ToString());

                CRMPortalRepository rep = new CRMPortalRepository();

                DataRepository.UpdateFirmLocation(firmLocation, CRMPortalUserID);

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), "OK", RequestDuration);
                result = new { Status = "Success" };

            }
            catch (Exception ex)
            {
                string message = ex.Message + Environment.NewLine + ex.StackTrace;

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), message, RequestDuration);
                result = new { Status = "Fail" };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);

        }

        [HttpPost]
        [ActionName("UpdateAttorneyToCTX")]
        public HttpResponseMessage UpdateAttorneyToCTX([FromBody]object value)
        {
            object result = null;
            string strMessageText = string.Empty;

            try
            {
                AttorneyInfo attorney = JsonConvert.DeserializeObject<AttorneyInfo>(value.ToString());

                CRMPortalRepository rep = new CRMPortalRepository();

                attorney.Birthday = attorney.Birthday == DateTime.MinValue? null: attorney.Birthday;
                attorney.BarVerificationDate = attorney.BarVerificationDate == DateTime.MinValue ? null : attorney.BarVerificationDate;
                attorney.BarVerificationExpirationDate = attorney.BarVerificationExpirationDate == DateTime.MinValue ? null : attorney.BarVerificationExpirationDate;

                //Get the Old AttorneyDetails info to log the changes
                var oldAttorneyInfo = DataRepository.GetAttorneyInfoByCRMID(attorney.CRM_AttorneyID);
                // This will track all the changes related to the Attorney.
                strMessageText = AllChanges(oldAttorneyInfo, attorney, UserID);

                if(!string.IsNullOrEmpty(strMessageText))
                {
                    DataRepository.LogPersonChanges(attorney.AttorneyID.ToString(), "Attorney", DateTime.Now, UserID, strMessageText);
                }

                DataRepository.UpdateAttorney(attorney, CRMPortalUserID);

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), "OK", RequestDuration);
                result = new { Status = "Success" };

            }
            catch (Exception ex)
            {
                string message = ex.Message + Environment.NewLine + ex.StackTrace;

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), message, RequestDuration);
                result = new { Status = "Fail" };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [HttpPost]
        [ActionName("UpdateAttorneyAssistantToCTX")]
        public HttpResponseMessage UpdateAttorneyAssistantToCTX([FromBody]object value)
        {
            object result = null;

            try
            {
                AttorneyAssistant attorneyAssistant = JsonConvert.DeserializeObject<AttorneyAssistant>(value.ToString());

                CRMPortalRepository rep = new CRMPortalRepository();

                DataRepository.UpdateAttorneyAssistant(attorneyAssistant, CRMPortalUserID);

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), "OK", RequestDuration);
                result = new { Status = "Success" };

            }
            catch (Exception ex)
            {
                string message = ex.Message + Environment.NewLine + ex.StackTrace;

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), message, RequestDuration);
                result = new { Status = "Fail" };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);

        }

        [HttpPost]
        [ActionName("MergeAttorneys")]
        public HttpResponseMessage MergeAttorneys(int FromAttorneyID, int ToAttorneyID)
        {
            object result = null;

            try
            {
                DataRepository.MergeAttorneyActivities(FromAttorneyID, ToAttorneyID, CRMPortalUserID);
                DataRepository.MergeAttorneys(FromAttorneyID, ToAttorneyID, CRMPortalUserID);

                CRMPortalLogger.LogRequest(UserID, Request, "From = " + FromAttorneyID.ToString() + ", To = " + ToAttorneyID.ToString(), "OK", RequestDuration);
                result = new { Status = "Success" };

            }
            catch (Exception ex)
            {
                string message = ex.Message + Environment.NewLine + ex.StackTrace;

                CRMPortalLogger.LogRequest(UserID, Request, "From = " + FromAttorneyID.ToString() + ", To = " + ToAttorneyID.ToString(), message, RequestDuration);
                result = new { Status = "Fail" };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);

        }

        [HttpPost]
        [ActionName("MoveAttorney")]
        public HttpResponseMessage MoveAttorney(int AttorneyID, int ToFirmLocationID)
        {
            object result = null;

            try
            {
                DataRepository.MoveAttorney(AttorneyID, ToFirmLocationID, CRMPortalUserID);

                CRMPortalLogger.LogRequest(UserID, Request, "AttorneyID=" + AttorneyID.ToString() + ", ToFirmLocationID=" + ToFirmLocationID.ToString(), "OK", RequestDuration);
                result = new { Status = "Success" };

            }
            catch (Exception ex)
            {
                string message = ex.Message + Environment.NewLine + ex.StackTrace;

                CRMPortalLogger.LogRequest(UserID, Request, "AttorneyID=" + AttorneyID.ToString() + ", ToFirmLocationID=" + ToFirmLocationID.ToString(), message, RequestDuration);
                result = new { Status = "Fail" };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);

        }

        [HttpPost]
        [ActionName("MergeFirmLocations")]
        public HttpResponseMessage MergeFirmLocations(int FromFirmLocationID, int ToFirmLocationID)
        {
            object result = null;

            try
            {
                DataRepository.MergeFirmLocations(FromFirmLocationID, ToFirmLocationID, CRMPortalUserID);

                CRMPortalLogger.LogRequest(UserID, Request, "From=" + FromFirmLocationID.ToString() + ", To=" + ToFirmLocationID.ToString(), "OK", RequestDuration);
                result = new { Status = "Success" };

            }
            catch (Exception ex)
            {
                string message = ex.Message + Environment.NewLine + ex.StackTrace;

                CRMPortalLogger.LogRequest(UserID, Request, "From=" + FromFirmLocationID.ToString() + ", To=" + ToFirmLocationID.ToString(), message, RequestDuration);
                result = new { Status = "Fail" };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);

        }

        [HttpPost]
        [ActionName("MergeFirms")]
        public HttpResponseMessage MergeFirms(int FromFirmID, int ToFirmID)
        {
            object result = null;

            try
            {
                DataRepository.MergeFirms(FromFirmID, ToFirmID, CRMPortalUserID);

                CRMPortalLogger.LogRequest(UserID, Request, "From=" + FromFirmID.ToString() + ", To=" + ToFirmID.ToString(), "OK", RequestDuration);
                result = new { Status = "Success" };

            }
            catch (Exception ex)
            {
                string message = ex.Message + Environment.NewLine + ex.StackTrace;

                CRMPortalLogger.LogRequest(UserID, Request, "From=" + FromFirmID.ToString() + ", To=" + ToFirmID.ToString(), message, RequestDuration);
                result = new { Status = "Fail" };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);

        }

        private string AllChanges(AttorneyInfo oldAttorneyInfo, AttorneyInfo newAttorneyInfo, string userLogon)
        {
            string strMessageText = string.Empty;

            if (oldAttorneyInfo.FirstName.Trim() != newAttorneyInfo.FirstName.Trim())
            {
                strMessageText += "First Name" + " changed from " + oldAttorneyInfo.FirstName + " to " + newAttorneyInfo.FirstName;
            }
            if (oldAttorneyInfo.LastName.Trim() != newAttorneyInfo.LastName.Trim())
            {
                strMessageText += "Last Name" + " changed from " + oldAttorneyInfo.LastName + " to " + newAttorneyInfo.LastName;
            }
            if (oldAttorneyInfo.Email.Trim() != newAttorneyInfo.Email.Trim())
            {
                strMessageText += "Email" + " changed from " + oldAttorneyInfo.Email + " to " + newAttorneyInfo.Email;
            }
            if (Oasis.Common.Functions.FormatPhone(oldAttorneyInfo.PhoneNumber) != Oasis.Common.Functions.FormatPhone(newAttorneyInfo.PhoneNumber))
            {
                strMessageText += "Phone Number" + " changed from " + oldAttorneyInfo.PhoneNumber + " to " + newAttorneyInfo.PhoneNumber;
            }
            if (Oasis.Common.Functions.FormatPhone(oldAttorneyInfo.Fax) != Oasis.Common.Functions.FormatPhone(newAttorneyInfo.Fax))
            {
                strMessageText += "Fax" + " changed from " + oldAttorneyInfo.Fax + " to " + newAttorneyInfo.Fax;
            }
            if (oldAttorneyInfo.Birthday.ToString().Trim() != newAttorneyInfo.Birthday.ToString().Trim())
            {
                strMessageText += "Birthday" + " changed from " + oldAttorneyInfo.Birthday.ToString() + " to " + newAttorneyInfo.Birthday.ToString();
            }
            if (oldAttorneyInfo.ContactInformation.Trim() != newAttorneyInfo.ContactInformation.Trim())
            {
                strMessageText += "Contact Information" + " changed from " + oldAttorneyInfo.ContactInformation + " to " + newAttorneyInfo.ContactInformation;
            }

            return strMessageText;
        }
    }
}


