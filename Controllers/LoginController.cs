﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Configuration;
using Newtonsoft.Json;


namespace CRMPortal
{
    public class LoginController : BaseController
    {
        [HttpPost]
        [ActionName("Login")]
        public HttpResponseMessage Login()
        {
            HttpRequestMessage request = this.Request;
            HttpResponseMessage result = new HttpResponseMessage();

            string message = string.Empty;

            if (!AuthenticateLoginRequest(request, out message))
            {
                CRMPortalLogger.LogRequest("", request, "", message, 0);

                var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                return response;
            }

            //by now email should be for a valid user
            string email = request.Headers.FirstOrDefault(h => h.Key.ToLower() == "email").Value.FirstOrDefault();
            int userId = GetUserID(email);
            string sessionId = StartNewSession(userId);

            object rm = new { Status = "Success", SessionID = sessionId };
            result = Request.CreateResponse(HttpStatusCode.OK, rm);
            return result;

        }

        private bool AuthenticateLoginRequest(HttpRequestMessage request, out string message)
        {
            message = string.Empty;

            // If there is no securitytoken in the header, we want to reject the request (return false) we should ALWAYS have a securitytoken from the CRM Portal
            if (!request.Headers.Any(h => h.Key.ToLower() == "securitytoken"))
            {
                message = "Missing token header key";
                return false;
            }

            // If we have got securitytoken now we need to make sure that it is the correct one.
            var requestToken = request.Headers.FirstOrDefault(h => h.Key.ToLower() == "securitytoken").Value.FirstOrDefault();
            string securityToken = ConfigurationManager.AppSettings["SecurityToken"];

            // If the securitytoken is incorrect reject the request.
            if (requestToken != securityToken)
            {
                message = "Invalid token";
                return false;
            }

            // If there is no email in the header, we want to reject the request (return false) we should ALWAYS have an email from the CRM Portal
            if (!request.Headers.Any(h => h.Key.ToLower() == "email"))
            {
                message = "Missing email header key";
                return false;
            }

            // If the email in the header is empty (""), we want to reject the request (return false) we should ALWAYS have an email from the CRM Portal
            string requestEmail = request.Headers.FirstOrDefault(h => h.Key.ToLower() == "email").Value.FirstOrDefault();
            if (requestEmail.Trim() == "")
            {
                message = "Missing email value";
                return false;
            }

            if (IsValidUser(requestEmail) == false)
            {
                message = "Invalid email";
                return false;
            }

            return true;
        }

        private static bool IsValidUser(string email)
        {
            return CRMPortalRepository.IsValidUser(email);
        }

        public static int GetUserID(string email)
        {
            return CRMPortalRepository.GetUserID(email);
        }

        public static string StartNewSession(int userId)
        {
            return CRMPortalRepository.StartNewSession(userId);
        }

    }


}
