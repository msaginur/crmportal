﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Oasis.CRMIntegration;

namespace CRMPortal.Controllers
{
    public class ActivityController : BaseController
    {
        //
        // GET: /Activity/

        [HttpPost]
        [ActionName("AddCRMActivity")]
        public HttpResponseMessage AddCRMActivity([FromBody]object value)
        {
            object result = null;
            int crmActivityID;

            try
            {
                CRMActivity crmActivity = JsonConvert.DeserializeObject<CRMActivity>(value.ToString());

                CRMPortalRepository rep = new CRMPortalRepository();

                //Record Activity int CRMActivity table
                string email = this.Request.Headers.FirstOrDefault(h => h.Key.ToLower() == "email").Value.FirstOrDefault();
                int userId = CommonCode.GetUserID(email);

                crmActivityID = DataRepository.AddCRMActivity(crmActivity, userId);

                //Setup Attorney Pitched Flag
                if (crmActivity.SubType == "Pitched Atty/Provider" || crmActivity.SubType == "Pitched Support Contact")
                {
                    DataRepository.SetupAttorneyPitchedFlag(crmActivity.CRM_AttorneyID, (userId == 0 ? CRMPortalUserID : userId));
                }

                //Setup Attorney Pitched Flag

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), "OK", RequestDuration);
                result = new { Status = "Success", CTX_ActivityID = crmActivityID };

            }
            catch (Exception ex)
            {
                string message = ex.Message + Environment.NewLine + ex.StackTrace;

                CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), message, RequestDuration);
                result = new { Status = "Fail" };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);

        }

    }
}
