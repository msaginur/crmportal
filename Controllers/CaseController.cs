﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Oasis.UW.BusinessLayer;
using System.Text;
using Oasis.CRMIntegration;

namespace CRMPortal
{
    public class CaseController : BaseController
    {
        [HttpPost]
        [ActionName("CreateCase")]
        public HttpResponseMessage CreateCase([FromBody]object value)
        {
            object response = null;

            try
            {

                NewCase newApp = JsonConvert.DeserializeObject<NewCase>(value.ToString());

                if (!DataValidation.IsValidAttorneyID(newApp.AttorneyID, out validationError))
                {
                    CRMPortalLogger.LogRequest(UserID, Request, newApp.AttorneyID.ToString(), "Fail", RequestDuration);
                    return Request.CreateResponse(HttpStatusCode.OK, validationError);
                }
                else
                {

                    DataRepository repository = new DataRepository();

                    int plaintiffID = repository.CreateNewPlaintiff(newApp.PlaintiffFirstName, newApp.PlaintiffLastName, newApp.Address1, newApp.Address2, newApp.City, newApp.State, newApp.ZipCode, newApp.Email, newApp.DOB, CRMPortalUserName, newApp.Phone);
                    int caseID = repository.CreateNewCase(newApp.CaseType, newApp.StateOfCourt, newApp.IncidentDate, newApp.AmountRequested, plaintiffID, newApp.AttorneyID, CRMPortalUserName, "Attorney Referral", 0);

                    string caseNote = CreateInitialCaseNote(newApp, UserID);

                    UWCaseInfo.AddCaseNote(caseID, caseNote, CRMPortalUserID, "CRMPortal");

                    repository.AssignCase(caseID, CRMPortalUserName);
                    repository.CreateApplication(caseID, plaintiffID, newApp.AttorneyID, newApp.IncidentDate, CRMPortalUserName);
                    repository.StartCommunicationFlow(caseID);
                    repository.UpdateAttorneyOnCase(caseID, newApp.AttorneyID, CRMPortalUserID);

                    List<Exception> validationException = new List<Exception>();
                    FraudControls.ValidatePossibleDuplicateCases(caseID, out validationException);

                    CRMPortalLogger.LogRequest(UserID, Request, value.ToString(), "OK", RequestDuration);
                    response = new { Status = "Success", CTX_CaseID = caseID };
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message + Environment.NewLine + ex.StackTrace;

                string parameter = string.Empty;
                parameter = value != null ? value.ToString() : string.Empty;

                CRMPortalLogger.LogRequest(UserID, Request, parameter, message, RequestDuration);
                response = new { Status = "Fail" };
            }

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }


        private string CreateInitialCaseNote(NewCase newApp, string AttyEmail)
        {
            StringBuilder sb = new StringBuilder();

            if (newApp.CaseType != null && newApp.CaseType != "")
            {
                sb.AppendLine(string.Format("<b>Application Date:</b> {0} <b>Case Type:</b> {1}<br>", DateTime.Now, newApp.CaseType));
            }

            sb.AppendLine(string.Format("<b>Posted By:</b> {0}<br>", CRMPortalUserName));
            sb.AppendLine(string.Format("<b>From CRM Portal:</b> {0}<br>", AttyEmail));

            if (newApp.AmountRequested > 0)
            {
                sb.AppendLine(string.Format("<b>Requested Amount:</b> {0}", newApp.AmountRequested));
            }
            if (newApp.IncidentDate != null && newApp.IncidentDate != "" && newApp.IncidentDate.Length > 3)
            {
                sb.AppendLine(string.Format("<b>Incident Date:</b> {0}<br>", newApp.IncidentDate));
            }

            sb.AppendLine(string.Format("<b>Case Details:</b><br>"));

            switch (newApp.CaseType.ToLower())
            {
                case "auto": //"Auto":
                    sb.AppendLine(string.Format("Auto<br>"));
                    if (newApp.PlaintiffInvolvement != null && newApp.PlaintiffInvolvement != "")
                    { 
                        sb.AppendLine(string.Format("{0}<br>", newApp.PlaintiffInvolvement)); 
                    }

                    if (newApp.AccidentType != null && newApp.AccidentType != "")
                    {
                        sb.AppendLine(string.Format("Accident Type: {0}<br>", newApp.AccidentType.ToString())); 
                    }

                    if (newApp.Injuries != null && newApp.Injuries != string.Empty)
                    {
                        sb.AppendLine(string.Format("Injuries: {0}<br>", newApp.Injuries.Replace(";", "; "))); 
                    }

                    if (newApp.OtherInjuries != null && newApp.OtherInjuries != "")
                    { 
                        sb.AppendLine(string.Format("Other Injureies: {0}<br>", newApp.OtherInjuries)); 
                    }

                    if (newApp.Treatments != null && newApp.Treatments != string.Empty)
                    {
                        sb.AppendLine(string.Format("Treatments: {0}<br>", newApp.Treatments.Replace(";", "; "))); 
                    }
                    
                    if (newApp.OtherTreatments != null && newApp.OtherTreatments != "")
                    { 
                        sb.AppendLine(string.Format("Other Treatments: {0}<br>", newApp.OtherTreatments)); 
                    }
                    
                    if (newApp.OfferAmount != null && newApp.OfferAmount != "")
                    { 
                        sb.AppendLine(string.Format("Offer Amount: {0}<br>", newApp.OfferAmount)); 
                    }

                    if (newApp.MoreThan1000Damage != null && newApp.MoreThan1000Damage != "")
                    {
                        sb.AppendLine(string.Format("$1000 property damage: {0}<br>", newApp.MoreThan1000Damage));
                    
                        if (newApp.WhosInsurancePaid != null && newApp.WhosInsurancePaid != "")
                        { 
                            sb.AppendLine(string.Format("Whos insutance paid: {0}<br>", newApp.WhosInsurancePaid)); 
                        }
                    }

                    if (newApp.AdditionalInformation != null && newApp.AdditionalInformation != "")
                    { 
                        sb.AppendLine(string.Format("Additional Info: {0}<br>", newApp.AdditionalInformation)); 
                    }

                    break;

                case "worker's compensation": //"Worker's Compensation":
                    sb.AppendLine(string.Format("Worker's Compensation<br>"));
                    if (newApp.Treatments != null && newApp.Treatments != string.Empty)
                    {
                        sb.AppendLine(string.Format("Treatments: {0}<br>", newApp.Treatments.Replace(";", "; ")));
                    }
                    if (newApp.TreatmentsDescription != null && newApp.TreatmentsDescription != "")
                    {
                        sb.AppendLine(string.Format("Other: {0}<br>", newApp.TreatmentsDescription));
                    }

                    if (newApp.OfferAmount != null && newApp.OfferAmount != "")
                    {
                        sb.AppendLine(string.Format("Offer Amount: {0}<br>", newApp.OfferAmount));
                    }

                    if (newApp.AdditionalInformation != null && newApp.AdditionalInformation != "")
                    {
                        sb.AppendLine(string.Format("Additional Info: {0}<br>", newApp.AdditionalInformation));
                    }

                    break;
                default:
                    sb.AppendLine(string.Format("Other<br>"));

                    if (newApp.AdditionalInformation != null && newApp.AdditionalInformation != "")
                    {
                        sb.AppendLine(string.Format("Additional Info: {0}<br>", newApp.AdditionalInformation));
                    }

                    break;

            }


            return sb.ToString();
        }

    }
}
