﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oasis.Common;
using System.Data;
using System.Data.SqlClient;

namespace CRMPortal
{
    public class CRMPortalRepository
    {
        private DataConnection dataConnection;

        public CRMPortalRepository()
        {
            dataConnection = new DataConnection();
        }

        public static int GetUserID(string email)
        {
            int userId = 0;

            var parms = new SqlParameter[] { 
                new SqlParameter("@Email", email) ,
            };

            DataConnection dataConnection = new DataConnection();
            var ds = dataConnection.ExecStoredProc("GetUserID", parms);

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                userId = ds.Tables[0].Rows[0].Field<int>("UserID");
            }

            return userId;
        }

        public static bool IsValidUser(string email)
        {
            int userId = 0;

            var parms = new SqlParameter[] { 
                new SqlParameter("@Email", email) ,
            };

            DataConnection dataConnection = new DataConnection();
            var ds = dataConnection.ExecStoredProc("IsValidUser", parms);

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                userId = ds.Tables[0].Rows[0].Field<int>("UserID");
            }

            return userId != 0;
        }

        public static string StartNewSession(int userId)
        {
            string sessionId = string.Empty;

            var parms = new SqlParameter[] { 
                new SqlParameter("@UserID", userId) ,
            };

            DataConnection dataConnection = new DataConnection();
            var ds = dataConnection.ExecStoredProc("CRM_StartNewSession", parms);

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                sessionId = ds.Tables[0].Rows[0].Field<string>("SessionID");
            }

            return sessionId;
        }

        public static bool IsSessionAlive(string sessionId)
        {
            bool sessionAlive = false;

            var parms = new SqlParameter[] { 
                new SqlParameter("@SessionID", sessionId) ,
            };

            DataConnection dataConnection = new DataConnection();
            var ds = dataConnection.ExecStoredProc("CRM_IsSessionAlive", parms);

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                sessionAlive = true;
            }

            return sessionAlive;
        }

        public static void RefreshSession(string sessionId)
        {

            var parms = new SqlParameter[] { 
                new SqlParameter("@SessionID", sessionId) ,
            };

            DataConnection dataConnection = new DataConnection();
            dataConnection.ExecNonQueryStoredProc("CRM_RefreshSession", parms);

        }
    }

}