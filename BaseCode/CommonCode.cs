﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CRMPortal
{
    public static class CommonCode
    {
        public static string AppSetting(string strSettingName)
        {
            string strResult;
            string runInTest = ConfigurationManager.AppSettings["Environment"];  // 1=Test   0=Live

            if (runInTest == "TEST")
            {
                // If this is a test 
                try
                {
                    // Get the test value if there is one
                    strResult = ConfigurationManager.AppSettings[strSettingName + "_Test"].ToString();
                }
                catch (System.NullReferenceException)
                {
                    // If there's no test value, get the regular value
                    strResult = ConfigurationManager.AppSettings[strSettingName];
                }
            }
            else
            {
                // If this is not a test, get the regular value
                strResult = ConfigurationManager.AppSettings[strSettingName];
            }

            return strResult;
        }
        public static int GetUserID(string email)
        {
            return CRMPortalRepository.GetUserID(email);
        }


    }
}