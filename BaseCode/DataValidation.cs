﻿using Oasis.CRMIntegration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace CRMPortal
{

    public class ValidationError
    {
        public string Status { get; set; }
        public int ErrNum { get; set; }
        public string Message { get; set; }
    }
    
    public class DataValidation 
    {
        /// <summary>
        /// Returns true if the CaseID exists in the database returns false if it does not
        /// </summary>
        /// <param name="CaseID"></param>
        /// <param name="validationError"></param>
        /// <returns></returns>
        public static bool IsValidCaseID(int CaseID, out ValidationError validationError)
        {
            validationError = null;

            bool retVal = false;
            DataRepository apr = new DataRepository();
            retVal = apr.IsValidCaseID(CaseID);

            if (!retVal)
            {
                validationError = new ValidationError
                {
                    Status = "Fail",
                    ErrNum = (int)DataValidationError.CaseNotFound,
                    Message = "CaseID " + CaseID.ToString() + " not found."
                };
            }

            return retVal;
        }

        public static bool IsValidFirmID(int FirmID, out ValidationError validationError)
        {
            validationError = null;

            bool retVal = false;
            DataRepository apr = new DataRepository();
            retVal = apr.IsValidFirmID(FirmID);

            if (!retVal)
            {
                validationError = new ValidationError
                {
                    Status = "Fail",
                    ErrNum = (int)DataValidationError.FirmNotFound,
                    Message = "FirmID " + FirmID.ToString() + " not found."
                };
            }

            return retVal;
        }

        public static bool IsValidAttorneyID(int AttorneyID, out ValidationError validationError)
        {
            validationError = null;

            bool retVal = false;
            DataRepository apr = new DataRepository();
            retVal = apr.IsValidAttorneyID(AttorneyID);

            if (!retVal)
            {
                validationError = new ValidationError
                {
                    Status = "Fail",
                    ErrNum = (int)DataValidationError.AttorneyNotFound,
                    Message = "AttorneyID " + AttorneyID.ToString() + " not found."
                };
            }

            return retVal;
        }

        public enum DataValidationError
        {
            CaseNotFound = 1,
            FirmNotFound = 2,
            AttorneyNotFound = 3,
            FirmAttorneysNotFound = 4
        }
    }
}