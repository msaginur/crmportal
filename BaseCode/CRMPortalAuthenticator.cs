﻿using Oasis.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace CRMPortal
{
    public class CRMPortalAuthenticator : DelegatingHandler
    {
        public CRMPortalAuthenticator()
        {
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            string message = string.Empty;

            if (!AuthenticateRequest(request, out message))
            {
                var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                var tsc = new TaskCompletionSource<HttpResponseMessage>();
                tsc.SetResult(response);

                CRMPortalLogger.LogRequest("", request, "", message, 0);

                return tsc.Task;
            }

            return base.SendAsync(request, cancellationToken);
        }

        /// <summary>
        /// This will make sure that the request that is coming to the API has all the correct criteria.
        /// The Email and SecurityToken are required and the token value should match the one we are using.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool AuthenticateRequest(HttpRequestMessage request, out string message)
        {
            message = string.Empty;

            if (request.RequestUri.AbsolutePath.ToLower() == "/api/login/login")
            {
                //the authentication is happening in the login request on the home controller
                return true;
            }

            // If there is no email in the header, we want to reject the request (return false) we should ALWAYS have an email from the CRM Portal
            if (!request.Headers.Any(h => h.Key.ToLower() == "email"))
            {
                message = "Missing email header key";
                return false;
            }

            // If the email in the header is empty (""), we want to reject the request (return false) we should ALWAYS have an email from the CRM Portal
            string requestEmail = request.Headers.FirstOrDefault(h => h.Key.ToLower() == "email").Value.FirstOrDefault();
            if (requestEmail.Trim() == "")
            {
                message = "Missing email value";
                return false;
            }

            // If the emailstring is not in the email format, reject the request (return false) we should have a valid email address of rht Attorney using the CRM Portal
            if (IsValidEmail(requestEmail) == false)
            {
                message = "Email string not in email format";
                return false;
            }


            // If there is no SessionID in the header, we want to reject the request (return false) we should ALWAYS have a SessionID from the CRM Portal
            if (!request.Headers.Any(h => h.Key.ToLower() == "sessionid"))
            {
                message = "Missing SessionID header key";
                return false;
            }

            // If we have gotten this far there is an email and a securitytoken now we need to make sure that it is the correct one.
            var sessionID = request.Headers.FirstOrDefault(h => h.Key.ToLower() == "sessionid").Value.FirstOrDefault();

            // If the securitytoken is incorrect reject the request.
            if (!IsSessionAlive(sessionID))
            {
                message = "Invalid Session ID";
                return false;
            }
            else // refresh the session
            {
                RefreshSession(sessionID);
            }

            var identity = new GenericIdentity(requestEmail);
            SetPrincipal(new GenericPrincipal(identity, null));

            return true;
        }

        /// <summary>
        /// this is a function to check if email is valid
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void SetPrincipal(IPrincipal principal)
        {
            Thread.CurrentPrincipal = principal;
            if (HttpContext.Current != null)
            {
                HttpContext.Current.User = principal;
            }
        }

        private bool IsSessionAlive(string sessionId)
        {
            return CRMPortalRepository.IsSessionAlive(sessionId);
        }

        private void RefreshSession(string sessionId)
        {
            CRMPortalRepository.RefreshSession(sessionId);
        }

    }
}