﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using System.Threading;
using Newtonsoft.Json;

namespace CRMPortal
{
    public class BaseController : ApiController
    {
        public DateTime requestStart;
        public string UserID = string.Empty;

        public int CRMPortalUserID = Convert.ToInt32(CommonCode.AppSetting("CRMPortalUserID"));
        public string CRMPortalUserName = CommonCode.AppSetting("CRMPortalUserName");
        
        public double RequestDuration
        {
            get { return DateTime.Now.Subtract(requestStart).TotalMilliseconds; }
        }

        public ValidationError validationError = new ValidationError();

        protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);

            requestStart = DateTime.Now;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                UserID = Thread.CurrentPrincipal.Identity.Name;
            }
        }

    }

    public class ReturnMessage
    {
        public string Status { get; set; }
        public object Data { get; set; }
    }

}
