﻿using Oasis.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace CRMPortal
{
    public class CRMPortalLogger
    {
        private DataConnection dataConnection;

        public CRMPortalLogger()
        {
            dataConnection = new DataConnection();
        }

        public static void LogRequest(string userId, HttpRequestMessage request, string parameter, string status, double duration)
        {
            if (request != null)
            {
                LogRequest(userId, request.RequestUri.LocalPath, request.Method.ToString(), parameter, request.Headers.ToString(), status, duration);
            }
            else
            {
                LogRequest(userId, "", "", parameter, "", status, duration);
            }

        }


        public static void LogRequest(string userId, string url, string method, string parameter, string requestHeaders, string status, double duration)
        {
            try
            {

                var parms = new SqlParameter[] { 
                new SqlParameter("@UserID", userId)
                , new SqlParameter("@URL", url) 
                , new SqlParameter("@Method", method) 
                , new SqlParameter("@Parameter", parameter) 
                , new SqlParameter("@RequestHeaders", requestHeaders) 
                , new SqlParameter("@Status", status) 
                , new SqlParameter("@Duration", duration) 
            
                };

                var dataConnection = new DataConnection();
                dataConnection.ExecNonQueryStoredProc("LogCRMPortalRequest", parms);
            }
            catch (Exception)
            {
            }
        }
    }
}