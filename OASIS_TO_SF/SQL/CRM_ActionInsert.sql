USE [Oasis]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CRM_ActionInsert]') AND type IN (N'P', N'PC') ) 
    DROP PROCEDURE [dbo].[CRM_ActionInsert]
GO

CREATE PROCEDURE [dbo].[CRM_ActionInsert](
	@CRM_ActionTypeID INT
	, @CRM_OasisID int
	, @CRM_ResponseInfo varchar(3000) = ''
	, @CRM_StatusID int = 0
	)
AS BEGIN     

	DECLARE @UserID INT, @CRM_ActionName VARCHAR(50)
	SELECT @UserID = UserID FROM usertable WHERE userlogon = SYSTEM_USER

	SELECT @CRM_ActionName = a.CRM_ActionTypeName
	FROM CRM_ActionType a 
	WHERE a.CRM_ActionTypeID = @CRM_ActionTypeID


	INSERT INTO [dbo].[CRM_Action]
			   ([CRM_ActionTypeID]
			   ,[CRM_ActionName]
			   ,[CRM_OasisID]
			   ,[CRM_ResponseInfo]
			   ,[CRM_StatusID]
			   ,[CRM_DateCreated]
			   ,[CRM_CreatedBy])
		 VALUES
			   (@CRM_ActionTypeID
			   ,@CRM_ActionName
			   ,@CRM_OasisID
			   ,@CRM_ResponseInfo
			   ,@CRM_StatusID
			   ,GETDATE()
			   ,@UserID)
END

GO

GRANT EXECUTE ON [dbo].[CRM_ActionInsert] TO [PUBLIC]
GO
