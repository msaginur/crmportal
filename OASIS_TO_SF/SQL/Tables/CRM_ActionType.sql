USE Oasis
GO
/*
   Monday, April 15, 20199:54:41 AM
   User: 
   Server: OLFSQLDEV4
   Database: Oasis
   Application: 
*/

DROP TABLE [dbo].[CRM_ActionType]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CRM_ActionType](
	[CRM_ActionTypeID] [int] NOT NULL,
	[CRM_ActionTypeName] [varchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL
) ON [PRIMARY]
GO

USE [Oasis]
GO

INSERT INTO [dbo].[CRM_ActionType]
           ([CRM_ActionTypeID]
           ,[CRM_ActionTypeName]
           ,[Active]
           ,[SortOrder])
     VALUES
           (1
           ,'CREATE_FIRM'
           ,1
           ,1)

INSERT INTO [dbo].[CRM_ActionType]
           ([CRM_ActionTypeID]
           ,[CRM_ActionTypeName]
           ,[Active]
           ,[SortOrder])
     VALUES
           (2
           ,'UPDATE_FIRM'
           ,1
           ,2)

INSERT INTO [dbo].[CRM_ActionType]
           ([CRM_ActionTypeID]
           ,[CRM_ActionTypeName]
           ,[Active]
           ,[SortOrder])
     VALUES
           (3
           ,'CREATE_ATTORNEY'
           ,1
           ,3)

INSERT INTO [dbo].[CRM_ActionType]
           ([CRM_ActionTypeID]
           ,[CRM_ActionTypeName]
           ,[Active]
           ,[SortOrder])
     VALUES
           (4
           ,'UPDATE_ATTORNEY'
           ,1
           ,4)

