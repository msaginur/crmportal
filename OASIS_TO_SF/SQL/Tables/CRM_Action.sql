USE [Oasis]
GO

/****** Object:  Table [dbo].[CRM_Action]    Script Date: 4/12/2019 2:41:22 PM ******/
DROP TABLE [dbo].[CRM_Action]
GO

/****** Object:  Table [dbo].[CRM_Action]    Script Date: 4/12/2019 2:41:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CRM_Action](
	[CRM_ActionID] [int] IDENTITY(1,1) NOT NULL,
	[CRM_ActionTypeID] [int] NOT NULL,
	[CRM_ActionName] [varchar](50) NOT NULL,
	[CRM_OasisID] [int] NOT NULL,
	[CRM_ResponseInfo] [varchar](3000) NOT NULL,
	[CRM_StatusID] [int] NOT NULL,
	[CRM_DateCreated] [datetime] NOT NULL,
	[CRM_CreatedBy] [int] NOT NULL,
	[CRM_DateUpdated] [datetime] NULL,
) ON [PRIMARY]
GO


