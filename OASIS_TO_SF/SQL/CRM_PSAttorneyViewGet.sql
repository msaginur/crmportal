USE [Oasis]
GO

/****** Object:  StoredProcedure [dbo].[CRM_PSAttorneyViewGet]    Script Date: 4/11/2016 1:25:18 PM ******/
DROP PROCEDURE [dbo].[CRM_PSAttorneyViewGet]
GO

/****** Object:  StoredProcedure [dbo].[CRM_PSAttorneyViewGet]    Script Date: 4/11/2016 1:25:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- EXEC [dbo].[CRM_PSAttorneyViewGet] 1219544 --1303935
CREATE PROCEDURE [dbo].[CRM_PSAttorneyViewGet]
	@attorneyID int 
AS BEGIN

	/************************************************************************

	Maintenance History:

	Date		Who	Description
	----------	---	---------------------------------------------------------
	04/30/2019	ab	Initial Implementation
	************************************************************************/

	--declare @attorneyId int
	--set @attorneyId = 1303935

	DECLARE @CRM_AttorneyID VARCHAR(50), @CRM_FirmID VARCHAR(50), @PS_FirmId INT

	SELECT @CRM_AttorneyID = a.CRM_ID, @CRM_FirmID = c.CRM_ID, @PS_FirmId = c.OrganizationID 
	FROM AttorneyDetails a
	join PersonOrganization b on a.AttorneyID = b.PersonID
	join Organization c on c.OrganizationID = b.OrganizationID
	WHERE a.AttorneyID = @attorneyID

	IF (ISNULL(@CRM_AttorneyID, '') = '' OR ISNULL(@CRM_FirmID, '') = '')
	BEGIN
		RAISERROR (N'Error: attorney CRM_ID or Firm CRM_ID is NULL. Attorney ID: %d.', -- Message text.  
           16, -- Severity,  
           1, -- State,  
           @attorneyID); -- Argument.  
		
		RETURN 
	END 

	/* key fields should be on every data pull
	CRM_FirmId
	CRM_AttorneyId
	PS_FirmId
	PS_AttorneyId
	*/
	select 
		 @CRM_FirmID as CRM_FirmId  -- Need datamodel to support
		,@CRM_AttorneyID as CRM_AttorneyId -- Need datamodel to support
		,isnull(i.organizationId, -999) PS_FirmId  -- need to fix data to add organization for EVERY attorney
		,a.attorneyid PS_AttorneyId
		,a.Grade
		,isnull(c.name, '') BookOfBusinessOwner
		,s.Name ServicingRepresentative
		-- need to get 'Temp Funding Hold'
		,case when a.blacklist = 1 then 'Blacklist' else '' end FundingHold
		,( select count(1) from vCaseAttorney z where z.personId = a.attorneyId) TotalCases
		, isnull(d.totalFundedCases, 0) totalFundedCount
		, isnull(d.totalFundedAmount, 0) totalFundedAmount
		, isnull(e.referralCount, 0) referralCount
		, isnull(e.totalReferralFundingAmount, 0) totalReferralFundingAmount
		, isnull(g.PricingDescription, '') defaultPricing
		, cast (null as float) RealizedMultiple 
		, cast (null as float) ContractMultiple
		, cast (null as float) MultipleRatio
		, cast (null as money) EstimatedNetProfit
	into #AttorneyFlat
	from attorney a
	left outer join Book_Of_Business b on a.AttorneyID = b.AttorneyID
		and b.ActiveFlag = 1
	left outer join UserTable c on c.userId = b.ACID
	join userTable s on s.userId = a.ServicingUserID
	left outer join (select z.personId, sum(fundingCount) totalFundedCount, sum(totalFunded) totalFundedAmount, count(1) totalFundedCases
			from vCaseAttorney z
			join vCaseTotalFunded y on z.caseId = y.caseId
			where z.personId = @attorneyId
			group by z.personId) d on d.personId = a.attorneyId
	left outer join (select z.personId, y.ReferralChannelID, count(distinct z.caseId) referralCount, sum(u.TotalFunded) totalReferralFundingAmount
			from vCaseAttorney z
			join vCaseInfoWithStatus y on z.caseId = y.caseId
			join ReferralChannel x on x.ReferralChannelID = y.ReferralChannelID
				and x.channelName = 'Attorney Referral'
			left outer join vCaseTotalFunded u on u.CaseID = y.CaseID
			where z.personId = @attorneyId
			group by z.personId, y.ReferralChannelID) e on e.personId = a.attorneyId
	join AttorneyDetails f on f.AttorneyID = a.AttorneyID
	left outer join Pricing g on g.PricingID = f.PreferredPricingID
	left outer join PersonOrganization h on h.personId = a.attorneyId
	left outer join organization i on i.OrganizationID = h.OrganizationID
	where a.attorneyId = @attorneyId

	--Attorney Report card (not on a table!)
	create table #attorneyReportCard
	(LastFundingDate varchar(20)
	, OpenCases varchar(20)
	, OpenDollars varchar(20)
	, OpenCasesResp varchar(20)
	, OpenDollarsResp varchar(20)
	, ImpairedCases varchar(20)
	, ImpairedDollars varchar(20)
	, ImpairedCasesResp varchar(20)
	, ImpairedDollarsResp varchar(20)
	, PaidInFullCases varchar(20)
	, PaidInFullAdvances varchar(20)
	, PaidInFullCasesResp varchar(20)
	, PaidInFullAdvancesResp varchar(20)
	, WrittenOffCases varchar(20)
	, WrittenOffAdvances varchar(20)
	, WrittenOffCasesResp varchar(20)
	, WrittenOffAdvancesResp varchar(20)
	, WaivedCases varchar(20)
	, WaivedAdvances varchar(20)
	, WaivedCasesResp varchar(20)
	, WaivedAdvancesResp varchar(20)
	, CollectionsCases varchar(20)
	, CollectionsValue varchar(20)
	, CollectionsCasesResp varchar(20)
	, CollectionsValueResp varchar(20)
	, TotalFundedCases varchar(20)
	, TotalFundedDollars varchar(20)
	, TotalFundedCasesResp varchar(20)
	, TotalFundedDollarsResp varchar(20)
	, ImpairedRatio varchar(20)
	, ImpairedDollarRatio varchar(20)
	, RealizedLossDollars varchar(20)
	, RealizedMultiple varchar(20)
	, ContractMultiple varchar(20)
	, MultipleRatio varchar(20)
	, AtRiskDollars varchar(20)
	, EstimatedNetProfit varchar(20)
	, HunterStatus varchar(100) )
	insert into #attorneyReportCard
	exec [dbo].[AttorneyReportCardShort]  @AttorneyID = @attorneyId, @ShowLawFirm = 0  

	--select --AttorneyID = @attorneyId
	--  convert(float, RealizedMultiple) RealizedMultiple
	--, convert(float, ContractMultiple) ContractMultiple
	--, convert(float, replace(MultipleRatio, '%', '')) / 100 MultipleRatio
	--, convert(money, replace(replace(EstimatedNetProfit, '$', ''), ',', '')) EstimatedNetProfit
	--
	update b set RealizedMultiple = convert(float, a.RealizedMultiple) 
	, ContractMultiple = convert(float, a.ContractMultiple)
	, MultipleRatio = convert(float, replace(a.MultipleRatio, '%', '')) / 100 
	, EstimatedNetProfit =  convert(money, replace(replace(a.EstimatedNetProfit, '$', ''), ',', ''))
	from #attorneyReportCard a
	, #AttorneyFlat b 

	-- First Data Set (PS View 360)
	-- Flat Result set
	select *
	from #AttorneyFlat 


	--last three initial fundings
	select top 3 
	 a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId
	, v.FullName, x.TransactionDate, c.GroupName PricingCategory, y2.SubTypeName CaseType
	--, rank() over ( partition by x.caseId order by x.transactionDate, x.transactionid ) FundingSequence
	from #AttorneyFlat a
	join vCaseAttorney z on z.personId = a.PS_AttorneyId
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join SubType y2 on y2.SubTypeId = y.CaseSubTypeID
	join transactions x on x.caseId = y.caseId
		and x.transactionTypeId = 181
	join vCasePlaintiff v on v.caseId = y.caseId
	join pricing b on x.PricingID = b.PricingID
	join EffectiveInterestGroup c on b.EffectiveInterestGroupID = c.EffectiveInterestGroupID
	order by rank() over ( partition by x.caseId order by x.transactionDate, x.transactionid ), x.transactionDate desc

	--Active/Approval Queue (PSApprovalQueue__c)
	select 
	 a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId
	, y.caseNumber
	, y.ApplicationStatus -- Add more info from Approval Queue
	, v.FullName PlaintiffName
	, z.caseId
	, y2.SubTypeName CaseType
	from #AttorneyFlat a
	join vCaseAttorney z on a.PS_AttorneyId = z.personId
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join SubType y2 on y2.SubTypeId = y.CaseSubTypeID
	join vCasePlaintiff v on v.caseId = y.caseId
	--join ApplicationStatusHistory y3 on y3.ApplicationStatusHistoryID = y.ApplicationStatusHistoryID
	where applicationStatusId < 3

	--Last 3 paid in full cases
	select top 3
	 a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	, y.caseNumber
	, v.FullName PlaintiffName
	, b.lastPaymentDate 
	, b.totalCollected
	from #AttorneyFlat a
	join vCaseAttorney z on z.PersonID = a.PS_AttorneyId
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join SubType y2 on y2.SubTypeId = y.CaseSubTypeID
	join vCasePlaintiff v on v.caseId = y.caseId
	join vCaseTotalCollected b on y.caseId = b.caseId
	where applicationStatusId = 3
	and paybackStatus = 'Paid in Full'
	order by b.lastPaymentDate desc


	-- Servicing Queue / My not be delivered talk to Servicing team
	select 
	 a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	, y.caseNumber
	, v.FullName PlaintiffName
	, y.servicingStatusDate, y.servicingStatus
	-- Need to Add stats here
	, null TotalFunded
	, null TotalOwed
	, null LastServicingNote
	from #AttorneyFlat a
	join vCaseAttorney z on z.PersonID = a.PS_AttorneyId 
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join SubType y2 on y2.SubTypeId = y.CaseSubTypeID
	join vCasePlaintiff v on v.caseId = y.caseId
	where applicationStatusId = 3
	and paybackStatus in ('open', 'impaired')
	order by y.servicingStatusDate desc

	-- Case Type BreakDown
	select 
	 a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	, y2.SubTypeName CaseTypeName, Count(1) CaseCount
	from #AttorneyFlat a
	join vCaseAttorney z on z.PersonID = a.PS_AttorneyId 
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join SubType y2 on y2.SubTypeId = y.CaseSubTypeID
	group by a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	, y2.SubTypeName
	order by 6 desc

	-- Case Source BreakDown
	select 
	 a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	, x.channelName CaseSource
	, count(1) CaseCount
	from #AttorneyFlat a
	join vCaseAttorney z on z.PersonID = a.PS_AttorneyId 
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join ReferralChannel x on x.ReferralChannelID = y.ReferralChannelID
	join SubType y2 on y2.SubTypeId = y.CaseSubTypeID
	where z.personId = @attorneyId
	group by a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	--, y2.SubTypeName
	, x.channelName
	order by 6 desc

	-- Pricing BreakDown
	-- Counts approvals, count is independent and can be higher then total cases
	select 
	 a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	,e.GroupName PricingType, count(1) CaseCount
	from #AttorneyFlat a
	join vCaseAttorney z on z.PersonID = a.PS_AttorneyId 
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join transactions c on c.caseId = y.caseId
		and c.transactionTypeId = 180 --approvals
	join pricing d on d.pricingId = c.PricingID
	join EffectiveInterestGroup e on e.EffectiveInterestGroupID = d.EffectiveInterestGroupID
	where z.personId = @attorneyId
	group by  a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	,e.GroupName
	order by 6 desc


	-- User Notifications
	-- TBD need meeting on this one - is this really useful?
	select c.*
	from vCaseAttorney z
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join userNotification c on c.caseId = y.caseId
	where z.personId = @attorneyId

	drop table #attorneyReportCard
	drop table #AttorneyFlat 

END


GO

