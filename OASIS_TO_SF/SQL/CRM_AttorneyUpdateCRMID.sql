USE [Oasis]
GO

/****** Object:  StoredProcedure [dbo].[CRM_AttorneyUpdateCRMID]    Script Date: 4/11/2016 1:25:18 PM ******/
DROP PROCEDURE [dbo].[CRM_AttorneyUpdateCRMID]
GO

/****** Object:  StoredProcedure [dbo].[CRM_AttorneyUpdateCRMID]    Script Date: 4/11/2016 1:25:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
CREATE PROCEDURE [dbo].[CRM_AttorneyUpdateCRMID]
	@AttorneyID int 
	, @CRMID VARCHAR(50)
AS BEGIN

	/************************************************************************

	Maintenance History:

	Date		Who	Description
	----------	---	---------------------------------------------------------
	04/10/2019	ab	Initial Implementation
	************************************************************************/

	UPDATE ad SET ad.CRM_ID = @CRMID
	FROM 
		AttorneyDetails AD 
	WHERE 
		AD.AttorneyID = @AttorneyID 
END


GO

