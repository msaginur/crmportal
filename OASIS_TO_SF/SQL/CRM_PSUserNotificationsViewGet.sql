USE [Oasis]
GO

/****** Object:  StoredProcedure [dbo].[CRM_PSUserNotificationsViewGet]    Script Date: 4/11/2016 1:25:18 PM ******/
DROP PROCEDURE [dbo].[CRM_PSUserNotificationsViewGet]
GO

/****** Object:  StoredProcedure [dbo].[CRM_PSUserNotificationsViewGet]    Script Date: 4/11/2016 1:25:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- EXEC [dbo].[CRM_PSUserNotificationsViewGet] 
CREATE PROCEDURE [dbo].[CRM_PSUserNotificationsViewGet]
AS BEGIN

	/************************************************************************

	Maintenance History:

	Date		Who	Description
	----------	---	---------------------------------------------------------
	04/30/2019	ab	Initial Implementation
	************************************************************************/

	/* key fields should be on every data pull
	CRM_FirmId
	CRM_AttorneyId
	PS_FirmId
	PS_AttorneyId
	*/
	select 
		 i.CRM_ID as CRM_FirmId  -- Need datamodel to support
		,f.CRM_ID as CRM_AttorneyId -- Need datamodel to support
		,isnull(i.organizationId, -999) PS_FirmId  -- need to fix data to add organization for EVERY attorney
		,a.attorneyid PS_AttorneyId
		,a.Grade
		,isnull(c.name, '') BookOfBusinessOwner
		,s.Name ServicingRepresentative
		-- need to get 'Temp Funding Hold'
		,case when a.blacklist = 1 then 'Blacklist' else '' end FundingHold
		,( select count(1) from vCaseAttorney z where z.personId = a.attorneyId) TotalCases
		, isnull(g.PricingDescription, '') defaultPricing
		, cast (null as float) RealizedMultiple 
		, cast (null as float) ContractMultiple
		, cast (null as float) MultipleRatio
		, cast (null as money) EstimatedNetProfit
	into #AttorneyFlat
	from attorney a
	left outer join Book_Of_Business b on a.AttorneyID = b.AttorneyID
		and b.ActiveFlag = 1
	left outer join UserTable c on c.userId = b.ACID
	join userTable s on s.userId = a.ServicingUserID
	join AttorneyDetails f on f.AttorneyID = a.AttorneyID
	left outer join Pricing g on g.PricingID = f.PreferredPricingID
	left outer join PersonOrganization h on h.personId = a.attorneyId
	left outer join organization i on i.OrganizationID = h.OrganizationID
	where ISNULL(f.CRM_ID , '') <> '' and  ISNULL(i.CRM_ID , '') <> ''

	-- User Notifications
	-- TBD need meeting on this one - is this really useful?
	select 
	 a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	, c.*
	from #AttorneyFlat a
	join vCaseAttorney z on z.PersonID = a.PS_AttorneyId 
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join userNotification c on c.caseId = y.caseId


	drop table #AttorneyFlat 

END


GO

