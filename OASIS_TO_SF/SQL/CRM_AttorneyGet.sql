USE [Oasis]
GO

/****** Object:  StoredProcedure [dbo].[CRM_AttorneyGet]    Script Date: 4/11/2016 1:25:18 PM ******/
DROP PROCEDURE [dbo].[CRM_AttorneyGet]
GO

/****** Object:  StoredProcedure [dbo].[CRM_AttorneyGet]    Script Date: 4/11/2016 1:25:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
CREATE PROCEDURE [dbo].[CRM_AttorneyGet]
	@AttorneyID int 
AS BEGIN

	/************************************************************************

	Maintenance History:

	Date		Who	Description
	----------	---	---------------------------------------------------------
	04/10/2019	ab	Initial Implementation
	************************************************************************/

	DECLARE @currAttorneyID int

	SELECT @currAttorneyID = @AttorneyID

	
	SELECT d.PersonID
		, LastName = ISNULL(d.LastName, '')
		, FirstName = ISNULL(d.FirstName, '')
		, MI = ISNULL(d.MiddleName, '')
		, d.Prefix
		, d.DateofBirth
		, Email = ISNULL(d.Email, '')
		, d.Watchlist
		, e.PrimaryAddress
		, f.Address1
		, f.Address2
		, f.address3
		, f.City
		, [State] = st.StateName
		, f.PostalCode
		, AttorneyContact = j.UserLogon
		, AttorneyContactName = j.FirstName + ' ' + j.LastName
		, ServicingRep = n.UserLogon
		, ServicingRepName = n.FirstName + ' ' + n.LastName
		, l.OrganizationID
		, LawFirm = ISNULL(l.organizationName, '')
		, d.PreferredEmailorFax
		, d.SpecialInstr
		, MoEmail = ISNULL(d.month_email, 0)
		, AG.AttorneyGradeID
		, AG.Grade
		, AG.ImageFile
		, d.topAtty
		, Uncooperative = ISNULL(AD.Uncooperative, 0)
		, DoNotModify = ISNULL(d.DoNotModify, 0)
		, SalesRep = ut.UserLogon
		, SalesRepName = ISNULL(ut.FirstName + ' ' + ut.LastName, '')
		, PhoneNumber = ISNULL(dbo.FormatPhone(ph.PhoneNumber), '')
		, FaxNumber = ISNULL(dbo.FormatPhone(fph.PhoneNumber), '')
		, Attorney_CRM_ID = ISNULL(AD.CRM_ID, '')
		, Firm_CRM_ID = ISNULL(l.CRM_ID, '')
		, BarVerificationDate = AD.barVerified
		, BarVerificationExpirationDate = ad.barVerificationExpires
	FROM Person d 
	INNER JOIN AttorneyDetails AD ON AD.AttorneyID = d.PersonID
	INNER JOIN AttorneyGrade AG ON AG.AttorneyGradeID = AD.AttorneyGradeID
	LEFT OUTER JOIN PersonAddress e ON d.PersonID = e.PersonId AND e.AddressTypeId = 105
	LEFT OUTER JOIN Address f ON e.AddressID = f.AddressID
	LEFT OUTER JOIN PersonPhone g ON d.PersonId = g.PersonId AND g.PhoneTypeId = 107
	LEFT OUTER JOIN UserTable j ON AD.AttorneyContactUserID = j.UserId
	LEFT OUTER JOIN PersonOrganization m ON d.PersonID = m.PersonId
	LEFT OUTER JOIN Organization l ON l.organizationID = m.OrganizationId
	LEFT OUTER JOIN UserTable n ON AD.ServicingUserID = n.UserId
	LEFT OUTER JOIN UserTable ut ON AD.OSRUserID = ut.UserId
	LEFT OUTER JOIN Phone ph ON ph.PhoneId = g.PhoneId
	LEFT OUTER JOIN PersonPhone pf ON d.PersonId = pf.PersonId AND pf.PhoneTypeId = 108
	LEFT OUTER JOIN Phone fph ON fph.PhoneId = pf.PhoneId
	left outer join StateTable st on f.State = st.StateAbr
	WHERE 
		d.PersonId = @currAttorneyID 
	AND d.PersonTypeId = 102
END


GO

