USE [Oasis]
GO

/****** Object:  StoredProcedure [dbo].[CRM_LawFirmGet]    Script Date: 4/11/2016 1:25:18 PM ******/
DROP PROCEDURE [dbo].[CRM_LawFirmGet]
GO

/****** Object:  StoredProcedure [dbo].[CRM_LawFirmGet]    Script Date: 4/11/2016 1:25:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
CREATE PROCEDURE [dbo].[CRM_LawFirmGet]
	@FirmID int 
AS BEGIN

	/************************************************************************

	Maintenance History:

	Date		Who	Description
	----------	---	---------------------------------------------------------
	04/10/2019	ab	Initial Implementation
	************************************************************************/

	SELECT 
		l.OrganizationID
		, l.OrganizationName
		, l.CRM_ID
		, PhoneNumber = c.PhoneNumber
		, FaxNumber = e.PhoneNumber
	FROM 
		Organization l 
		left outer join OrganizationPhone b on l.OrganizationID = b.OrganizationID
			and b.PhoneType = 107
		left outer join Phone c on c.PhoneID = b.PhoneID
		left outer join OrganizationPhone f on l.OrganizationID = f.OrganizationID
			and f.PhoneType = 108
		left outer join Phone e on e.PhoneID = f.PhoneID
	WHERE 
		l.OrganizationID = @FirmID
END


GO

