USE [Oasis]
GO

/****** Object:  StoredProcedure [dbo].[CRM_PSViewGet]    Script Date: 4/11/2016 1:25:18 PM ******/
DROP PROCEDURE [dbo].[CRM_PSViewGet]
GO

/****** Object:  StoredProcedure [dbo].[CRM_PSViewGet]    Script Date: 4/11/2016 1:25:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- EXEC [dbo].[CRM_PSViewGet] 1219544 --1303935
CREATE PROCEDURE [dbo].[CRM_PSViewGet]
	@attorneyID int 
AS BEGIN

	/************************************************************************

	Maintenance History:

	Date		Who	Description
	----------	---	---------------------------------------------------------
	04/30/2019	ab	Initial Implementation
	************************************************************************/

	--declare @attorneyId int
	--set @attorneyId = 1303935

	DECLARE @CRM_AttorneyID VARCHAR(50), @CRM_FirmID VARCHAR(50), @PS_FirmId INT

	SELECT @CRM_AttorneyID = a.CRM_ID, @CRM_FirmID = c.CRM_ID, @PS_FirmId = c.OrganizationID 
	FROM AttorneyDetails a
	join PersonOrganization b on a.AttorneyID = b.PersonID
	join Organization c on c.OrganizationID = b.OrganizationID
	WHERE a.AttorneyID = @attorneyID

	IF (ISNULL(@CRM_AttorneyID, '') = '' OR ISNULL(@CRM_FirmID, '') = '')
	BEGIN
		RAISERROR (N'Error: attorney CRM_ID or Firm CRM_ID is NULL. Attorney ID: %d.', -- Message text.  
           16, -- Severity,  
           1, -- State,  
           @attorneyID); -- Argument.  
		
		RETURN 
	END 

	/* key fields should be on every data pull
	CRM_FirmId
	CRM_AttorneyId
	PS_FirmId
	PS_AttorneyId
	*/
	select 
		 @CRM_FirmID as CRM_FirmId  -- Need datamodel to support
		,@CRM_AttorneyID as CRM_AttorneyId -- Need datamodel to support
		,isnull(i.organizationId, -999) PS_FirmId  -- need to fix data to add organization for EVERY attorney
		,a.attorneyid PS_AttorneyId
		,a.Grade
		,isnull(c.name, '') BookOfBusinessOwner
		,s.Name ServicingRepresentative
		-- need to get 'Temp Funding Hold'
		,case when a.blacklist = 1 then 'Blacklist' else '' end FundingHold
		,( select count(1) from vCaseAttorney z where z.personId = a.attorneyId) TotalCases
		, isnull(d.totalFundedCases, 0) totalFundedCount
		, isnull(d.totalFundedAmount, 0) totalFundedAmount
		, isnull(e.referralCount, 0) referralCount
		, isnull(e.totalReferralFundingAmount, 0) totalReferralFundingAmount
		, isnull(g.PricingDescription, '') defaultPricing
		, cast (null as float) RealizedMultiple 
		, cast (null as float) ContractMultiple
		, cast (null as float) MultipleRatio
		, cast (null as money) EstimatedNetProfit
	into #AttorneyFlat
	from attorney a
	left outer join Book_Of_Business b on a.AttorneyID = b.AttorneyID
		and b.ActiveFlag = 1
	left outer join UserTable c on c.userId = b.ACID
	join userTable s on s.userId = a.ServicingUserID
	left outer join (select z.personId, sum(fundingCount) totalFundedCount, sum(totalFunded) totalFundedAmount, count(1) totalFundedCases
			from vCaseAttorney z
			join vCaseTotalFunded y on z.caseId = y.caseId
			where z.personId = @attorneyId
			group by z.personId) d on d.personId = a.attorneyId
	left outer join (select z.personId, y.ReferralChannelID, count(distinct z.caseId) referralCount, sum(u.TotalFunded) totalReferralFundingAmount
			from vCaseAttorney z
			join vCaseInfoWithStatus y on z.caseId = y.caseId
			join ReferralChannel x on x.ReferralChannelID = y.ReferralChannelID
				and x.channelName = 'Attorney Referral'
			left outer join vCaseTotalFunded u on u.CaseID = y.CaseID
			where z.personId = @attorneyId
			group by z.personId, y.ReferralChannelID) e on e.personId = a.attorneyId
	join AttorneyDetails f on f.AttorneyID = a.AttorneyID
	left outer join Pricing g on g.PricingID = f.PreferredPricingID
	left outer join PersonOrganization h on h.personId = a.attorneyId
	left outer join organization i on i.OrganizationID = h.OrganizationID
	where a.attorneyId = @attorneyId

	--Attorney Report card (not on a table!)
	create table #attorneyReportCard
	(LastFundingDate varchar(20)
	, OpenCases varchar(20)
	, OpenDollars varchar(20)
	, OpenCasesResp varchar(20)
	, OpenDollarsResp varchar(20)
	, ImpairedCases varchar(20)
	, ImpairedDollars varchar(20)
	, ImpairedCasesResp varchar(20)
	, ImpairedDollarsResp varchar(20)
	, PaidInFullCases varchar(20)
	, PaidInFullAdvances varchar(20)
	, PaidInFullCasesResp varchar(20)
	, PaidInFullAdvancesResp varchar(20)
	, WrittenOffCases varchar(20)
	, WrittenOffAdvances varchar(20)
	, WrittenOffCasesResp varchar(20)
	, WrittenOffAdvancesResp varchar(20)
	, WaivedCases varchar(20)
	, WaivedAdvances varchar(20)
	, WaivedCasesResp varchar(20)
	, WaivedAdvancesResp varchar(20)
	, CollectionsCases varchar(20)
	, CollectionsValue varchar(20)
	, CollectionsCasesResp varchar(20)
	, CollectionsValueResp varchar(20)
	, TotalFundedCases varchar(20)
	, TotalFundedDollars varchar(20)
	, TotalFundedCasesResp varchar(20)
	, TotalFundedDollarsResp varchar(20)
	, ImpairedRatio varchar(20)
	, ImpairedDollarRatio varchar(20)
	, RealizedLossDollars varchar(20)
	, RealizedMultiple varchar(20)
	, ContractMultiple varchar(20)
	, MultipleRatio varchar(20)
	, AtRiskDollars varchar(20)
	, EstimatedNetProfit varchar(20)
	, HunterStatus varchar(100) )
	insert into #attorneyReportCard
	exec [dbo].[AttorneyReportCardShort]  @AttorneyID = @attorneyId, @ShowLawFirm = 0  


	update b set RealizedMultiple = convert(float, a.RealizedMultiple) 
	, ContractMultiple = convert(float, a.ContractMultiple)
	, MultipleRatio = convert(float, replace(a.MultipleRatio, '%', '')) / 100 
	, EstimatedNetProfit =  convert(money, replace(replace(a.EstimatedNetProfit, '$', ''), ',', ''))
	from #attorneyReportCard a
	, #AttorneyFlat b 

	-- First Data Set (PS View 360)
	-- Flat Result set
	select *
	from #AttorneyFlat 

	drop table #attorneyReportCard
	drop table #AttorneyFlat 

END


GO

