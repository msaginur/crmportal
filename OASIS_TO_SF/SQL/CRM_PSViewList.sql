USE [Oasis]
GO

/****** Object:  StoredProcedure [dbo].[CRM_PSViewAttorneyList]    Script Date: 4/11/2016 1:25:18 PM ******/
DROP PROCEDURE [dbo].[CRM_PSViewAttorneyList]
GO

/****** Object:  StoredProcedure [dbo].[CRM_PSViewAttorneyList]    Script Date: 4/11/2016 1:25:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- EXEC [dbo].[CRM_PSViewAttorneyList] 
CREATE PROCEDURE [dbo].[CRM_PSViewAttorneyList]
AS BEGIN

	/************************************************************************

	Maintenance History:

	Date		Who	Description
	----------	---	---------------------------------------------------------
	04/30/2019	ab	Initial Implementation
	************************************************************************/

	SELECT a.AttorneyID
	FROM AttorneyDetails a
	WHERE ISNULL(a.CRM_ID, '') <> ''
	ORDER BY a.AttorneyID

END
GO

GRANT EXECUTE ON [dbo].[CRM_PSViewAttorneyList] TO [public]
GO


