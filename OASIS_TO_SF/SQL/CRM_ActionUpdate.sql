USE [Oasis]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CRM_ActionUpdate]') AND type IN (N'P', N'PC') ) 
    DROP PROCEDURE [dbo].[CRM_ActionUpdate]
GO

CREATE PROCEDURE [dbo].[CRM_ActionUpdate](
	@CRM_ActionID INT
	, @CRM_ResponseInfo varchar(3000)
	, @CRM_StatusID int
	)
AS BEGIN     

	UPDATE  a
	   SET [CRM_ResponseInfo] = @CRM_ResponseInfo
		  ,[CRM_StatusID] = @CRM_StatusID
		  ,[CRM_DateUpdated] = GETDATE()
	FROM [dbo].[CRM_Action] a
	WHERE a.[CRM_ActionID] = @CRM_ActionID

END
GO

GRANT EXECUTE ON [dbo].[CRM_ActionUpdate] TO [PUBLIC]
GO
