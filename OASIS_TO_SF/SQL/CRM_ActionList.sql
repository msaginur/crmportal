USE [Oasis]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CRM_ActionList]') AND type IN (N'P', N'PC') ) 
    DROP PROCEDURE [dbo].[CRM_ActionList]
GO

CREATE PROCEDURE [dbo].[CRM_ActionList]
AS BEGIN     

	SELECT [CRM_ActionID]
		  ,[CRM_ActionName] = b.CRM_ActionTypeName
		  ,[CRM_OasisID]
		  ,[CRM_ResponseInfo]
		  ,[CRM_StatusID]
	  FROM [dbo].[CRM_Action] a
	  join CRM_ActionType b on b.CRM_ActionTypeID = a.CRM_ActionTypeID
	WHERE a.CRM_StatusID = 0
	ORDER BY b.SortOrder, a.CRM_ActionID

END
GO

GRANT EXECUTE ON [dbo].[CRM_ActionList] TO [PUBLIC]
GO
