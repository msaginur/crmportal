USE [Oasis]
GO

/****** Object:  StoredProcedure [dbo].[CRM_LawFirmUpdateCRMID]    Script Date: 4/11/2016 1:25:18 PM ******/
DROP PROCEDURE [dbo].[CRM_LawFirmUpdateCRMID]
GO

/****** Object:  StoredProcedure [dbo].[CRM_LawFirmUpdateCRMID]    Script Date: 4/11/2016 1:25:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 
CREATE PROCEDURE [dbo].[CRM_LawFirmUpdateCRMID]
	@FirmID int 
	, @CRMID VARCHAR(50)
AS BEGIN

	/************************************************************************

	Maintenance History:

	Date		Who	Description
	----------	---	---------------------------------------------------------
	04/10/2019	ab	Initial Implementation
	************************************************************************/

	UPDATE ad SET ad.CRM_ID = @CRMID
	FROM 
		Organization AD 
	WHERE 
		AD.OrganizationID = @FirmID
END


GO

