USE [Oasis]
GO

/****** Object:  StoredProcedure [dbo].[CRM_PSApprovalQueueViewGet]    Script Date: 4/11/2016 1:25:18 PM ******/
DROP PROCEDURE [dbo].[CRM_PSApprovalQueueViewGet]
GO

/****** Object:  StoredProcedure [dbo].[CRM_PSApprovalQueueViewGet]    Script Date: 4/11/2016 1:25:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- EXEC [dbo].[CRM_PSApprovalQueueViewGet] 
CREATE PROCEDURE [dbo].[CRM_PSApprovalQueueViewGet]
AS BEGIN

	/************************************************************************

	Maintenance History:

	Date		Who	Description
	----------	---	---------------------------------------------------------
	04/30/2019	ab	Initial Implementation
	************************************************************************/

	/* key fields should be on every data pull
	CRM_FirmId
	CRM_AttorneyId
	PS_FirmId
	PS_AttorneyId
	*/
	select 
		 i.CRM_ID as CRM_FirmId  -- Need datamodel to support
		,f.CRM_ID as CRM_AttorneyId -- Need datamodel to support
		,isnull(i.organizationId, -999) PS_FirmId  -- need to fix data to add organization for EVERY attorney
		,a.attorneyid PS_AttorneyId
		,a.Grade
		,isnull(c.name, '') BookOfBusinessOwner
		,s.Name ServicingRepresentative
		-- need to get 'Temp Funding Hold'
		,case when a.blacklist = 1 then 'Blacklist' else '' end FundingHold
		,( select count(1) from vCaseAttorney z where z.personId = a.attorneyId) TotalCases
		, isnull(g.PricingDescription, '') defaultPricing
		, cast (null as float) RealizedMultiple 
		, cast (null as float) ContractMultiple
		, cast (null as float) MultipleRatio
		, cast (null as money) EstimatedNetProfit
	into #AttorneyFlat
	from attorney a
	left outer join Book_Of_Business b on a.AttorneyID = b.AttorneyID
		and b.ActiveFlag = 1
	left outer join UserTable c on c.userId = b.ACID
	join userTable s on s.userId = a.ServicingUserID
	join AttorneyDetails f on f.AttorneyID = a.AttorneyID
	left outer join Pricing g on g.PricingID = f.PreferredPricingID
	left outer join PersonOrganization h on h.personId = a.attorneyId
	left outer join organization i on i.OrganizationID = h.OrganizationID
	where ISNULL(f.CRM_ID , '') <> '' and  ISNULL(i.CRM_ID , '') <> ''

	--Active/Approval Queue (PSApprovalQueue__c)
	select 
	 a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId
	, y.caseNumber
	, y.ApplicationStatus -- Add more info from Approval Queue
	, v.FullName PlaintiffName
	, z.caseId
	, y2.SubTypeName CaseType
	from #AttorneyFlat a
	join vCaseAttorney z on a.PS_AttorneyId = z.personId
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join SubType y2 on y2.SubTypeId = y.CaseSubTypeID
	join vCasePlaintiff v on v.caseId = y.caseId
	where applicationStatusId < 3

/*


	--Last 3 paid in full cases
	select top 3
	 a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	, y.caseNumber
	, v.FullName PlaintiffName
	, b.lastPaymentDate 
	, b.totalCollected
	from #AttorneyFlat a
	join vCaseAttorney z on z.PersonID = a.PS_AttorneyId
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join SubType y2 on y2.SubTypeId = y.CaseSubTypeID
	join vCasePlaintiff v on v.caseId = y.caseId
	join vCaseTotalCollected b on y.caseId = b.caseId
	where applicationStatusId = 3
	and paybackStatus = 'Paid in Full'
	order by b.lastPaymentDate desc


	-- Servicing Queue / My not be delivered talk to Servicing team
	select 
	 a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	, y.caseNumber
	, v.FullName PlaintiffName
	, y.servicingStatusDate, y.servicingStatus
	-- Need to Add stats here
	, null TotalFunded
	, null TotalOwed
	, null LastServicingNote
	from #AttorneyFlat a
	join vCaseAttorney z on z.PersonID = a.PS_AttorneyId 
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join SubType y2 on y2.SubTypeId = y.CaseSubTypeID
	join vCasePlaintiff v on v.caseId = y.caseId
	where applicationStatusId = 3
	and paybackStatus in ('open', 'impaired')
	order by y.servicingStatusDate desc

	-- Case Type BreakDown
	select 
	 a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	, y2.SubTypeName CaseTypeName, Count(1) CaseCount
	from #AttorneyFlat a
	join vCaseAttorney z on z.PersonID = a.PS_AttorneyId 
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join SubType y2 on y2.SubTypeId = y.CaseSubTypeID
	group by a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	, y2.SubTypeName
	order by 6 desc

	-- Case Source BreakDown
	select 
	 a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	, x.channelName CaseSource
	, count(1) CaseCount
	from #AttorneyFlat a
	join vCaseAttorney z on z.PersonID = a.PS_AttorneyId 
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join ReferralChannel x on x.ReferralChannelID = y.ReferralChannelID
	join SubType y2 on y2.SubTypeId = y.CaseSubTypeID
	group by a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	--, y2.SubTypeName
	, x.channelName
	order by 6 desc

	-- Pricing BreakDown
	-- Counts approvals, count is independent and can be higher then total cases
	select 
	 a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	,e.GroupName PricingType, count(1) CaseCount
	from #AttorneyFlat a
	join vCaseAttorney z on z.PersonID = a.PS_AttorneyId 
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join transactions c on c.caseId = y.caseId
		and c.transactionTypeId = 180 --approvals
	join pricing d on d.pricingId = c.PricingID
	join EffectiveInterestGroup e on e.EffectiveInterestGroupID = d.EffectiveInterestGroupID
	group by  a.CRM_FirmId
	,a.CRM_AttorneyId 
	,a.PS_FirmId  
	,a.PS_AttorneyId 
	,e.GroupName
	order by 6 desc


	-- User Notifications
	-- TBD need meeting on this one - is this really useful?
	select c.*
	from vCaseAttorney z
	join vCaseInfoWithStatus y on z.caseId = y.caseId
	join userNotification c on c.caseId = y.caseId
*/

	drop table #AttorneyFlat 

END
GO

