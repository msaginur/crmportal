﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Salesforce.Force;
using Salesforce.Common;
using Salesforce.Common.Models.Json;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using Oasis.SFCommunication;
using System.Data;
using System.Net.Http;

namespace SF_Test
{
    class Program
    {
        static void Main(string[] args)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls; // It will not work without this

            //LogMessage("-----------------------------------------------------------------------------------------------");
            //HTTest();
            //TestHttpAsync();
            //LogMessage("Start Main");
            MainAsync(args).Wait(); //After we change everything to normal calls - remove this async / wait calls
            //LogMessage("End Main");
        }

        static void HTTest()
        {

            string sfdcConsumerKey = ConfigurationManager.AppSettings["consumerkey"];
            string sfdcConsumerSecret = ConfigurationManager.AppSettings["consumersecret"];
            string sfdcUserName = ConfigurationManager.AppSettings["username"];
            string sfdcPassword = ConfigurationManager.AppSettings["password"];
            string sfdcToken = ConfigurationManager.AppSettings["initialtoken"];

            string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];
            string loginPassword = sfdcPassword + sfdcToken;

            string AuthToken;
            string InstanceUrl;

            var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                ? ConfigurationManager.AppSettings["SF_URL_test"]
                : ConfigurationManager.AppSettings["SF_URL_prod"];

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "POST";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";

            StringBuilder body = new StringBuilder();
            body.Append("grant_type=password");
            body.Append("&client_id=" + sfdcConsumerKey);
            body.Append("&client_secret=" + sfdcConsumerSecret);
            body.Append("&username=" + sfdcUserName);
            body.Append("&password=" + sfdcPassword);
            
            // Add parameters to post
            byte[] data = System.Text.Encoding.ASCII.GetBytes(body.ToString());
            httpWebRequest.ContentLength = data.Length;
            System.IO.Stream os = httpWebRequest.GetRequestStream();
            os.Write(data, 0, data.Length);
            os.Close();

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            Console.WriteLine("Response" + httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine(result.Replace(":{", ":\n{").Replace(",\"", ",\n\""));


                var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(result);
                AuthToken = values["access_token"];
                InstanceUrl = values["instance_url"];
            }

            var client = new HttpClient();

            string restRequest = InstanceUrl + "/services/apexrest/Preset/createCTXAttorney";
            var request = new HttpRequestMessage(HttpMethod.Post, restRequest);

            request.Headers.Add("Authorization", "Bearer " + AuthToken);
            request.Headers.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            request.Headers.Add("X-PrettyPrint", "1");

            StringBuilder sbody = new StringBuilder();
            sbody.Append("{\"CRM_FirmID\":\"0014B00000eRWSNQA4\",\"FirstName\":\"Philip\",\"CTX_FirmID\":\"167\",\"LastName\":\"Slotnick\",\"MiddleName\":\"J.\",\"Address1\":\"211 S. Florida Ave\",\"Address2\":\"\",\"City\":\"Lakeland\",\"State\":\"Florida\",\"Zip\":\"33801\",\"DayPhone\":\"8636888288\",\"Fax\":\"8636881978\",\"Email\":\"phil@burnetti.com\",\"AttorneyID\":\"42863\",\"Type\":\"Attorney\"}");

            var content = new System.Net.Http.StringContent(sbody.ToString(), Encoding.UTF8, "application/json");
            request.Content = content;
            var response = client.SendAsync(request).Result;
            
            string xStr = response.Content.ReadAsStringAsync().Result;
            LogMessage(xStr);
            Attorney_Response xResp =  JsonConvert.DeserializeObject<Attorney_Response>(xStr);
         }

        static async void TestHttpAsync()
        {
            HttpClient authClient = new HttpClient();
            string sfdcConsumerKey = ConfigurationManager.AppSettings["consumerkey"];
            string sfdcConsumerSecret = ConfigurationManager.AppSettings["consumersecret"];
            string sfdcUserName = ConfigurationManager.AppSettings["username"];
            string sfdcPassword = ConfigurationManager.AppSettings["password"];
            string sfdcToken = ConfigurationManager.AppSettings["initialtoken"];

            string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];


            var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                ? ConfigurationManager.AppSettings["SF_URL_test"]
                : ConfigurationManager.AppSettings["SF_URL_prod"];



            //create login password value
            string loginPassword = sfdcPassword + sfdcToken;

            HttpContent content = new FormUrlEncodedContent(new Dictionary<string, string>
  {
     {"grant_type","password"},
     {"client_id",sfdcConsumerKey},
     {"client_secret",sfdcConsumerSecret},
     {"username",sfdcUserName},
     {"password",loginPassword}
   }
            );

            //var keyValues = new List<KeyValuePair<string, string>>();
            //keyValues.Add(new KeyValuePair<string, string>("grant_type", "password"));
            //keyValues.Add(new KeyValuePair<string, string>("client_id", sfdcConsumerKey));
            //keyValues.Add(new KeyValuePair<string, string>("client_secret", sfdcConsumerSecret));
            //keyValues.Add(new KeyValuePair<string, string>("username", sfdcUserName));
            //keyValues.Add(new KeyValuePair<string, string>("password", loginPassword));

            //HttpContent content = new FormUrlEncodedContent(keyValues);

            try
            {
                HttpResponseMessage message = await authClient.PostAsync(url, content);


                string responseString = await message.Content.ReadAsStringAsync();

                object obj = JsonConvert.DeserializeObject(responseString);
            }
            catch (Exception ex)
            {
                LogMessage("?????? " + ex.Message);
            }
            //JObject obj = JObject.Parse(responseString);
            //oauthToken = (string)obj["access_token"];
            //serviceUrl = (string)obj["instance_url"];


            /* ============================================================================== */
            /*
                        HttpClient createClient = new HttpClient();

                        //string requestMessage = "{\"Name\":\"DevForce20\", \"AccountNumber\":\"1005555\"}";
                        //HttpContent content = new StringContent(requestMessage, Encoding.UTF8, "application/json");

                        string requestMessage = "<root><name>DevForce21</name><accountnumber>8994432</accountnumber></root>";
                        content = new StringContent(requestMessage, Encoding.UTF8, "application/xml");

                        string uri = serviceUrl + "/services/data/v25.0/sobjects/Account";

                        //create request message associated with POST verb
                        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, uri);

                        //add token to header
                        request.Headers.Add("Authorization", "Bearer " + oauthToken);

                        //return xml to the caller
                        request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                        request.Content = content;

                        HttpResponseMessage response = await createClient.SendAsync(request);

                        string result = await response.Content.ReadAsStringAsync();
                        */

        }

        static async Task MainAsync(string[] args)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls; // It will not work without this
            LogMessage("Start MainAsync");

            //CreatePSView();
            //CreatePSLastFundedCasesView();
            //CreatePSApprovalQueueView();
            //CreatePSPaidInFullView();
            //CreatePSCaseForServicingView();
            //CreatePSCaseTypeBreakdownView();
            //CreatePSCaseSourceBreakdownView();
            CreatePSPricingBreakdownView();
            return;

            //CreateAttorney(5700, 41609);

            StoredProcedure procList = new StoredProcedure("CRM_ActionList");
            DataSet dsList = procList.GetData();

            if (dsList.Tables.Count > 0)
            {
                if (dsList.Tables[0].Rows.Count > 0)
                {
                    int iStopCount = dsList.Tables[0].Rows.Count;

                    for (int iCount = 0; iCount < iStopCount; iCount++)
                    {
                        DataRow Row = dsList.Tables[0].Rows[iCount];
                        string strActionName = Row["CRM_ActionName"].ToString() ?? "";
                        int iCRM_OasisID = Row["CRM_OasisID"].ToString() == null ? -1 : Convert.ToInt32(Row["CRM_OasisID"]);
                        int iCRM_ActionID = Row["CRM_ActionID"].ToString() == null ? -1 : Convert.ToInt32(Row["CRM_ActionID"]);

                        LogMessage("strActionName: " + strActionName + " == iCRM_OasisID: " + iCRM_OasisID.ToString() + " == iCRM_ActionID: " + iCRM_ActionID.ToString());

                        switch (strActionName.ToUpper())
                        {
                            case "CREATE_ATTORNEY":
                                CreateAttorney(iCRM_OasisID, iCRM_ActionID);
                                break;
                            case "UPDATE_ATTORNEY":
                                UpdateAttorney(iCRM_OasisID, iCRM_ActionID);
                                break;
                            case "CREATE_FIRM":
                                CreateFirm(iCRM_OasisID, iCRM_ActionID);
                                break;
                            case "UPDATE_FIRM":
                                UpdateFirm(iCRM_OasisID, iCRM_ActionID);
                                break;
                        }
                    }
                }
            }
            LogMessage("End MainAsync");

            #region Commented out

            //firm.Name = "[x] " + firm.Name;
            //firm.UpdateLawFirmInCRM().Wait();

            //Oasis.SFCommunication.Attorney atty = new Oasis.SFCommunication.Attorney(308128);

            //atty.UpdateAttorneyInCRM().Wait();
            //atty.CreateAttorneyInCRM().Wait();


            //Do().Wait();
            //AddUser().Wait();

            ////Oasis.SFCommunication.Attorney xAttorney = new Oasis.SFCommunication.Attorney();

            //Console.WriteLine("-----------------------------");

            //Attorney attorney = new Attorney();
            //attorney = attorney.getAttorneyProfile(123);
            //string parameters = JsonConvert.SerializeObject(attorney);
            //Console.WriteLine(parameters.Replace(":{", ":\n{").Replace(",\"", ",\n\""));

            //Console.WriteLine("-----------------------------");

            //Payload_CreateAttorneyFirm payload = new Payload_CreateAttorneyFirm();
            //payload = payload.getPayload_CreateAttorneyFirm(654833, 123456);
            //parameters = JsonConvert.SerializeObject(payload);
            //Console.WriteLine(parameters.Replace(":{", ":\n{").Replace(",\"", ",\n\""));

            //Console.WriteLine("-----------------------------");

            //Payload_CreateAttorneyExisitngFirm payloadExisting = new Payload_CreateAttorneyExisitngFirm();
            //payloadExisting = payloadExisting.getPayload_CreateAttorneyFirm(654833, 123456);
            //parameters = JsonConvert.SerializeObject(payloadExisting);
            //Console.WriteLine(parameters.Replace(":{", ":\n{").Replace(",\"", ",\n\""));
            #endregion
        }

        public static void LogMessage(string message)
        {
            string strMessage = DateTime.Now.ToString("yyyyMMdd HH:mm:ss -- ") + message;
            Console.WriteLine(strMessage);

            DateTime dtNow = DateTime.Now;

            string fileName = AppDomain.CurrentDomain.BaseDirectory + "\\" + dtNow.ToString("yyyyMMdd") + ".log";

            using (StreamWriter log = File.AppendText(fileName))
            {
                log.WriteLine(strMessage);
                log.Close();
            }
        }

        public static string CreateExceptionString(Exception e)
        {
            StringBuilder sb = new StringBuilder();
            CreateExceptionString(sb, e, string.Empty);

            return sb.ToString();
        }

        private static void CreateExceptionString(StringBuilder sb, Exception e, string indent)
        {
            if (indent == null)
            {
                indent = string.Empty;
            }
            else if (indent.Length > 0)
            {
                sb.AppendFormat("{0}Inner ", indent);
            }

            sb.AppendFormat("Exception Found:\n{0}Type: {1}", indent, e.GetType().FullName);
            sb.AppendFormat("\n{0}Message: {1}", indent, e.Message);
            sb.AppendFormat("\n{0}Source: {1}", indent, e.Source);
            sb.AppendFormat("\n{0}Stacktrace: {1}", indent, e.StackTrace);

            if (e.InnerException != null)
            {
                sb.Append("\n");
                CreateExceptionString(sb, e.InnerException, indent + "  ");
            }
        }

        public static async void CreateFirm(int iFirmID, int iActionID)
        {
            Oasis.SFCommunication.LawFirm firm = new Oasis.SFCommunication.LawFirm(iFirmID, iActionID);
            try
            {
                await firm.CreateLawFirmInCRM();
                firm.LogResults();
            }
            catch (Exception ex)
            {
                // Handle exceptions.
                LogMessage("Exception in MainAsync: " + CreateExceptionString(ex));
            }
        }

        public static async void UpdateFirm(int iFirmID, int iActionID)
        {
            Oasis.SFCommunication.LawFirm firm = new Oasis.SFCommunication.LawFirm(iFirmID, iActionID);

            try
            {
                await firm.UpdateLawFirmInCRM();
                firm.LogResults();
            }
            catch (Exception ex)
            {
                // Handle exceptions.
                LogMessage("Exception in MainAsync: " + CreateExceptionString(ex));
            }
        }

        public static void CreateAttorney(int iAttorneyID, int iActionID)
        {
            Oasis.SFCommunication.Attorney atty = new Oasis.SFCommunication.Attorney(iAttorneyID, iActionID);

            try
            {
                atty.CreateAttorneyInCRM();
                atty.LogResults();
            }
            catch (Exception ex)
            {
                // Handle exceptions.
                LogMessage("Exception in MainAsync: " + CreateExceptionString(ex));
            }
        }

        public static void UpdateAttorney(int iAttorneyID, int iActionID)
        {
            Oasis.SFCommunication.Attorney atty = new Oasis.SFCommunication.Attorney(iAttorneyID, iActionID);

            try
            {
                atty.UpdateAttorneyInCRM();
                atty.LogResults();
            }
            catch (Exception ex)
            {
                // Handle exceptions. Or not.
                LogMessage("Exception in MainAsync: " + CreateExceptionString(ex));
            }
        }


        public static async void CreatePSView()
        {
            Oasis.SFCommunication.ViewsOperations viewOps = new ViewsOperations();

            try
            {
                await viewOps.CreatePSView();
                //firm.LogResults();
            }
            catch (Exception ex)
            {
                // Handle exceptions.
                LogMessage("Exception in MainAsync: " + CreateExceptionString(ex));
            }
        }

        public static async void CreatePSLastFundedCasesView()
        {
            Oasis.SFCommunication.ViewsOperations viewOps = new ViewsOperations();

            try
            {
                await viewOps.CreatePSLastFundedCasesView();
                //firm.LogResults();
            }
            catch (Exception ex)
            {
                // Handle exceptions.
                LogMessage("Exception in MainAsync: " + CreateExceptionString(ex));
            }
        }

        public static async void CreatePSApprovalQueueView()
        {
            Oasis.SFCommunication.ViewsOperations viewOps = new ViewsOperations();

            try
            {
                await viewOps.CreatePSApprovalQueueView();
                //firm.LogResults();
            }
            catch (Exception ex)
            {
                // Handle exceptions.
                LogMessage("Exception in MainAsync: " + CreateExceptionString(ex));
            }
        }

        public static async void CreatePSPaidInFullView()
        {
            Oasis.SFCommunication.ViewsOperations viewOps = new ViewsOperations();

            try
            {
                await viewOps.CreatePSPaidInFullView();
                //firm.LogResults();
            }
            catch (Exception ex)
            {
                // Handle exceptions.
                LogMessage("Exception in MainAsync: " + CreateExceptionString(ex));
            }
        }

        public static async void CreatePSCaseForServicingView()
        {
            Oasis.SFCommunication.ViewsOperations viewOps = new ViewsOperations();

            try
            {
                await viewOps.CreatePSCaseForServicingView();
                //firm.LogResults();
            }
            catch (Exception ex)
            {
                // Handle exceptions.
                LogMessage("Exception in MainAsync: " + CreateExceptionString(ex));
            }
        }



        public static async void CreatePSCaseTypeBreakdownView()
        {
            Oasis.SFCommunication.ViewsOperations viewOps = new ViewsOperations();

            try
            {
                await viewOps.CreatePSCaseTypeBreakdownView();
                //firm.LogResults();
            }
            catch (Exception ex)
            {
                // Handle exceptions.
                LogMessage("Exception in MainAsync: " + CreateExceptionString(ex));
            }
        }

        public static async void CreatePSCaseSourceBreakdownView()
        {
            Oasis.SFCommunication.ViewsOperations viewOps = new ViewsOperations();

            try
            {
                await viewOps.CreatePSCaseSourceBreakdownView();
                //firm.LogResults();
            }
            catch (Exception ex)
            {
                // Handle exceptions.
                LogMessage("Exception in MainAsync: " + CreateExceptionString(ex));
            }
        }

        public static async void CreatePSPricingBreakdownView()
        {
            Oasis.SFCommunication.ViewsOperations viewOps = new ViewsOperations();

            try
            {
                await viewOps.CreatePSPricingBreakdownView();
                //firm.LogResults();
            }
            catch (Exception ex)
            {
                // Handle exceptions.
                LogMessage("Exception in MainAsync: " + CreateExceptionString(ex));
            }
        }
        
        #region More comments


        static async Task Do()
        {
            //get credential values
            string consumerkey = ConfigurationManager.AppSettings["consumerkey"];
            string consumersecret = ConfigurationManager.AppSettings["consumersecret"];
            string username = ConfigurationManager.AppSettings["username"];
            string password = ConfigurationManager.AppSettings["password"];
            string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];

            var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                ? "https://test.salesforce.com/services/oauth2/token"
                : "https://login.salesforce.com/services/oauth2/token";

            //create auth client to retrieve token
            var auth = new AuthenticationClient();

            //get back URL and token
            await auth.UsernamePasswordAsync(consumerkey, consumersecret, username, password, url);

            var instanceUrl = auth.InstanceUrl;
            var accessToken = auth.AccessToken;
            var apiVersion = auth.ApiVersion;

            Console.WriteLine("instanceUrl: " + instanceUrl);
            Console.WriteLine("accessToken: " + accessToken);
            Console.WriteLine("apiVersion: " + apiVersion);

            string endPOINTURL = String.Format("https://test.salesforce.com/services/data/v44.0/sobjects/Account");
            endPOINTURL = String.Format(instanceUrl + "/services/data/" + apiVersion + "/sobjects/Account");

            Console.WriteLine("endPOINTURL: " + endPOINTURL);
            Console.WriteLine("-----------------------------");

            //var httpWebRequest = (HttpWebRequest)WebRequest.Create(endPOINTURL);

            //httpWebRequest.Method = "GET";
            //httpWebRequest.Accept = "application/Json";
            ////httpWebRequest.Headers.Add("Content-Type", "application/json");
            //httpWebRequest.Headers.Add("Authorization", "Bearer " + accessToken);

            //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //Console.WriteLine("Response" + httpResponse);
            //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //{
            //    var result = streamReader.ReadToEnd();
            //    //Console.WriteLine("XXX: " + result);
            //    Console.WriteLine(result.Replace(":{", ":\n{").Replace(",\"", ",\n\""));
            //}


            var httpWebRequest = (HttpWebRequest)WebRequest.Create(endPOINTURL);
            httpWebRequest.Method = "POST";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.Headers.Add("Authorization", "Bearer " + accessToken);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"Name\" : \"Express Logistics and Transport\"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            Console.WriteLine("Response" + httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine(result.Replace(":{", ":\n{").Replace(",\"", ",\n\""));
            }

        }

        public static async Task AddUser()
        {
            string consumerKey = ConfigurationManager.AppSettings["consumerkey"];
            string consumerSecret = ConfigurationManager.AppSettings["consumersecret"];
            string username = ConfigurationManager.AppSettings["username"];
            string password = ConfigurationManager.AppSettings["password"];
            string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];


            var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                ? ConfigurationManager.AppSettings["SF_URL_test"]
                : ConfigurationManager.AppSettings["SF_URL_prod"];

            //create auth client to retrieve token
            var auth = new AuthenticationClient();

            //get back URL and token
            await auth.UsernamePasswordAsync(consumerKey, consumerSecret, username, password, url);

            //const string AllowedChars =
            //        "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz#@$^*()";

            const string AllowedChars =
                    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";


            const string AllowedNums =
                    "0123456789";

            var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
            var cnt = new Contact();
            Random rng = new Random();
            cnt.FirstName = "FN-" + RandomString(AllowedChars, 3, 16, rng).ToLower();

            cnt.LastName = "LN-" + RandomString(AllowedChars, 5, 20, rng).ToLower();
            cnt.MiddleName__c = "MN-" + RandomString(AllowedChars, 1, 5, rng).ToLower();
            cnt.Email = "abondarenko@oasisfinancial.com";
            cnt.Title = "Title-" + RandomString(AllowedChars, 8, 20, rng).ToLower();

            cnt.Phone = RandomString(AllowedNums, 10, 10, rng);

            cnt.AccountID = "0014B00000eQWiaQAG";

            SuccessResponse resp = await client.CreateAsync("Contact", cnt);
        }

        private static string RandomString(string allowedChars, int minLength, int maxLength, Random rng)
        {
            char[] chars = new char[maxLength];
            int setLength = allowedChars.Length;

            int length = rng.Next(minLength, maxLength + 1);

            for (int i = 0; i < length; ++i)
            {
                chars[i] = allowedChars[rng.Next(setLength)];
            }

            return new string(chars, 0, length);
        }

        public class Contact
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Title { get; set; }
            //public string Birthday { get; set; }
            public string Phone { get; set; }
            public string AccountID { get; set; }
            public string MiddleName__c { get; set; }
        }
        #endregion 
    }
}
