﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Salesforce.Force;
using Salesforce.Common;
using Salesforce.Common.Models.Json;
using System.Configuration;
using System.Data;//.SqlClient;
using System.IO;

namespace Oasis.SFCommunication
{
    public class LawFirm
    {
        public const string SObjectTypeName = "Account";
        public string CTXFirmId__c { get; set; } // 
        public string Name { get; set; } // 
        public string Type { get; set; } // 
        public string Phone { get; set; }
        public string Fax { get; set; }

        string OasisFirmId { get; set; }
        string ID { get; set; } // CRM ID
        string CRM_ActionID { get; set; } // Task ID
        string Result { get; set; } // s
        int ResultID { get; set; } // 

        public LawFirm()
        {
            CTXFirmId__c = "";
            Name = "";
            ID = "";
            Phone = "";
            Fax = "";
        }

        public LawFirm(int iFirmID, int iCRM_ActionID)
        {
            StoredProcedure proc = new StoredProcedure("CRM_LawFirmGet");
            proc.AddParameter("@FirmID", iFirmID);
            DataSet ds = proc.GetData();

            if ((ds.Tables.Count > 0) && (ds.Tables[0].Rows.Count > 0))
            {
                DataRow Row = ds.Tables[0].Rows[0];

                OasisFirmId = iFirmID.ToString();
                CTXFirmId__c = iFirmID.ToString();
                Name = Row["OrganizationName"].ToString() ?? ""; // Row["FirstName"].ToString() == null ? "" : Row["FirstName"].ToString();
                Type = "Lawfirm";
                Phone = Row["PhoneNumber"].ToString() == null ? "" : Row["PhoneNumber"].ToString().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();
                Fax = Row["FaxNumber"].ToString() == null ? "" : Row["FaxNumber"].ToString().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();

                CRM_ActionID = iCRM_ActionID.ToString();

                if (Row["CRM_ID"] == System.DBNull.Value)
                {
                }
                else
                {
                    ID = Row["CRM_ID"].ToString();
                }
            }
            else
            {
                CTXFirmId__c = "";
                Name = "";
                ID = "";
                Type = "";
                Phone = "";
                Fax = "";
            }

        }

        public async Task CreateLawFirmInCRM()
        {
            if(this.Name != "" && this.ID == null)
            {
                string consumerKey = ConfigurationManager.AppSettings["consumerkey"];
                string consumerSecret = ConfigurationManager.AppSettings["consumersecret"];
                string username = ConfigurationManager.AppSettings["username"];
                string password = ConfigurationManager.AppSettings["password"];
                string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];

                var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                    ? ConfigurationManager.AppSettings["SF_URL_test"]
                    : ConfigurationManager.AppSettings["SF_URL_prod"];

                //create auth client to retrieve token
                var auth = new AuthenticationClient();

                //get back URL and token
                try
                {
                    auth.UsernamePasswordAsync(consumerKey, consumerSecret, username, password, url).Wait();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                try
                {
                    var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                    var tsk = client.CreateAsync(SObjectTypeName, this);
                    tsk.Wait();

                    SuccessResponse resp = tsk.Result;
                    if (resp.Success)
                    {
                        this.UpdateLawFirmCRMID(resp.Id);
                    }

                    this.Result = "Oasis ID: " + this.OasisFirmId + " | CRM ID: " + resp.Id;
                    this.ResultID = 1;
                }
                catch(Exception ex)
                {
                    string strEx = CreateExceptionString(ex);
                    this.Result = strEx;
                    this.ResultID = 2;
                    LogMessage("EXCEPTION: "+strEx);
                }
            }
            else
            {
                if (this.Name == "" )
                {
                    this.Result = "Name is not specified";
                }
                else
                {
                    if (this.ID != null)
                    {
                        this.Result = "ID is not null";
                    }
                    else
                    {
                        this.Result = "This is unhandled scenario";
                    }
                }

                this.Result = "CREATE_FIRM: " + this.Result;
                this.ResultID = 2;
            }
        }

        public async Task UpdateLawFirmInCRM()
        {
            if (this.Name != "" && this.ID != "" && this.ID != null)
            {
                string consumerKey = ConfigurationManager.AppSettings["consumerkey"];
                string consumerSecret = ConfigurationManager.AppSettings["consumersecret"];
                string username = ConfigurationManager.AppSettings["username"];
                string password = ConfigurationManager.AppSettings["password"];
                string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];

                var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                    ? ConfigurationManager.AppSettings["SF_URL_test"]
                    : ConfigurationManager.AppSettings["SF_URL_prod"];

                //create auth client to retrieve token
                var auth = new AuthenticationClient();

                //get back URL and token
                try
                {
                    auth.UsernamePasswordAsync(consumerKey, consumerSecret, username, password, url).Wait();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }


                try
                {
                    var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                    var tsk = client.UpdateAsync(SObjectTypeName, this.ID, this);
                    tsk.Wait();

                    SuccessResponse resp = tsk.Result;
                    if (resp.Success)
                    {
                        //this.UpdateLawFirmCRMID(resp.Id);
                    }

                    this.Result = "Oasis ID: " + this.OasisFirmId + " | CRM ID: " + resp.Id;
                    this.ResultID = 1;
                }
                catch (Exception ex)
                {
                    string strEx = CreateExceptionString(ex);
                    this.Result = strEx;
                    this.ResultID = 2;
                    LogMessage("EXCEPTION: " + strEx);
                }
            }
            else
            {
                if (this.Name == "")
                {
                    this.Result = "Firm Name is empty string";
                }
                else
                {
                    if (this.ID == "")
                    {
                        this.Result = "Firm CRM ID is empty string";
                    }
                    else
                    {
                        if (this.ID == null)
                        {
                            this.Result = "Firm CRM ID is NULL value";
                        }
                        else
                        {
                            this.Result = "Unhandled scernario";

                        }
                    }
                }
                this.Result = "UPDATE_FIRM: " + this.Result;
                this.ResultID = 2;

            }
        }

        void UpdateLawFirmCRMID(string strCRMID)
        {
            StoredProcedure proc = new StoredProcedure("CRM_LawFirmUpdateCRMID");
            proc.AddParameter("@FirmID", this.OasisFirmId);
            proc.AddParameter("@CRMID", strCRMID);
            proc.ExecuteNonQuery();
        }

        public void LogResults()
        {
            //Console.WriteLine("LogResults: " + this.Result);
            StoredProcedure proc = new StoredProcedure("CRM_ActionUpdate");
            proc.AddParameter("@CRM_ActionID", this.CRM_ActionID);
            proc.AddParameter("@CRM_ResponseInfo", this.Result);
            proc.AddParameter("@CRM_StatusID", this.ResultID);
            proc.GetData();

        }

        public static void LogMessage(string message)
        {
            string strMessage = DateTime.Now.ToString("yyyyMMdd HH:mm:ss -- ") + message;
            Console.WriteLine(strMessage);

            DateTime dtNow = DateTime.Now;

            string fileName = AppDomain.CurrentDomain.BaseDirectory + "\\" + dtNow.ToString("yyyyMMdd") + ".log";

            using (StreamWriter log = File.AppendText(fileName))
            {
                log.WriteLine(strMessage);
                log.Close();
            }
        }

        public static string CreateExceptionString(Exception e)
        {
            StringBuilder sb = new StringBuilder();
            CreateExceptionString(sb, e, string.Empty);

            return sb.ToString();
        }

        private static void CreateExceptionString(StringBuilder sb, Exception e, string indent)
        {
            if (indent == null)
            {
                indent = string.Empty;
            }
            else if (indent.Length > 0)
            {
                sb.AppendFormat("{0}Inner ", indent);
            }

            sb.AppendFormat("Exception Found:\n{0}Type: {1}", indent, e.GetType().FullName);
            sb.AppendFormat("\n{0}Message: {1}", indent, e.Message);
            //sb.AppendFormat("\n{0}Source: {1}", indent, e.Source);
            //sb.AppendFormat("\n{0}Stacktrace: {1}", indent, e.StackTrace);

            if (e.InnerException != null)
            {
                sb.Append("\n");
                CreateExceptionString(sb, e.InnerException, indent + "  ");
            }
        }


    }
}
