﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Salesforce.Force;
using Salesforce.Common;
using Salesforce.Common.Models.Json;
using System.Configuration;
using System.Data;
using System.IO;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;

namespace Oasis.SFCommunication
{
    public class Attorney
    {
        public const string SObjectTypeName = "Contact";

        public string CRM_FirmID { get; set; }
        public string FirstName { get; set; }
        public string CTX_FirmID { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string DayPhone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string AttorneyID { get; set; }
        public string Type { get; set; }
        public string CRM_AttorneyID { get; set; }
        public string CTX_AttorneyID { get; set; }


        string OasisAttorneyId { get; set; }
        string ID { get; set; }

        string Result { get; set; } // 
        string CRM_ActionID { get; set; }
        int ResultID { get; set; } // 

        string AuthToken;
        string InstanceUrl;

        public Attorney()
        {
            CRM_FirmID = "";
            FirstName = "";
            CTX_FirmID = "";
            LastName = "";
            MiddleName = "";
            Address1 = "";
            Address2 = "";
            City = "";
            State = "";
            Zip = "";
            DayPhone = "";
            Fax = "";
            Email = "";
            AttorneyID = "";
            Type = "";

        }

        public Attorney(int iAttorneyID, int iCRM_ActionID)
        {
            StoredProcedure proc = new StoredProcedure("CRM_AttorneyGet");
            proc.AddParameter("@AttorneyID", iAttorneyID);
            DataSet ds = proc.GetData();

            if ((ds.Tables.Count > 0) && (ds.Tables[0].Rows.Count > 0))
            {
                DataRow Row = ds.Tables[0].Rows[0];

                if (Row["Firm_CRM_ID"] == System.DBNull.Value)
                {
                    CRM_FirmID = "";
                }
                else
                {
                    CRM_FirmID = Row["Firm_CRM_ID"].ToString();
                }
                if (Row["Attorney_CRM_ID"].ToString() != "")
                {
                    CRM_AttorneyID = Row["Attorney_CRM_ID"].ToString();
                }
                FirstName = Row["FirstName"].ToString() ?? "";
                CTX_FirmID = Row["OrganizationID"].ToString() ?? "";
                LastName = Row["LastName"].ToString() ?? "";
                MiddleName = Row["MI"].ToString() ?? "";
                Address1 = Row["Address1"].ToString() ?? "";
                Address2 = Row["Address2"].ToString() ?? "";
                City = Row["City"].ToString() ?? "";
                State = Row["State"].ToString() ?? "";
                Zip = Row["PostalCode"].ToString() ?? "";
                DayPhone = Row["PhoneNumber"].ToString() == null ? "" : Row["PhoneNumber"].ToString().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();
                Fax = Row["FaxNumber"].ToString() == null ? "" : Row["FaxNumber"].ToString().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim();
                Email = Row["Email"].ToString() ?? "";
                AttorneyID = iAttorneyID.ToString();
                CTX_AttorneyID = iAttorneyID.ToString();
                Type = "Attorney";

                OasisAttorneyId = iAttorneyID.ToString();
                CRM_ActionID = iCRM_ActionID.ToString();
            }
            else
            {
                CRM_FirmID = "";
                FirstName = "";
                CTX_FirmID = "";
                LastName = "";
                MiddleName = "";
                Address1 = "";
                Address2 = "";
                City = "";
                State = "";
                Zip = "";
                DayPhone = "";
                Fax = "";
                Email = "";
                AttorneyID = "";
                Type = "";
            }
        }

        void UpdateAttorneyCRMID(string strCRMID)
        {
            StoredProcedure proc = new StoredProcedure("CRM_AttorneyUpdateCRMID");
            proc.AddParameter("@AttorneyID", this.OasisAttorneyId);
            proc.AddParameter("@CRMID", strCRMID);
            proc.ExecuteNonQuery();
        }

        public void CreateAttorneyInCRM()
        {
            LogMessage("CreateAttorneyInCRM = " + this.AttorneyID);

            if (this.ID == null || this.ID == "")
            {
                LogMessage("Call to SalesForce");

                var json = JsonConvert.SerializeObject(this, Formatting.None);
                var jsonI = JsonConvert.SerializeObject(this, Formatting.Indented);

                LogMessage("JSON:\n " + jsonI);

                SF_Login();

                var client = new HttpClient();

                string restRequest = this.InstanceUrl + "/services/apexrest/Preset/createCTXAttorney";
                var request = new HttpRequestMessage(HttpMethod.Post, restRequest);

                request.Headers.Add("Authorization", "Bearer " + this.AuthToken);
                request.Headers.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //request.Headers.Add("X-PrettyPrint", "1");

                var content = new System.Net.Http.StringContent(json, Encoding.UTF8, "application/json");
                request.Content = content;
                var response = client.SendAsync(request).Result;

                string xStr = response.Content.ReadAsStringAsync().Result;
                LogMessage(xStr);

                Attorney_Response xResp = JsonConvert.DeserializeObject<Attorney_Response>(xStr);

                if (xResp.CRM_AttorneyID != "")
                {
                    this.UpdateAttorneyCRMID(xResp.CRM_AttorneyID);
                    this.Result = "Success";
                    this.ResultID = 1;
                }
                else
                {
                    this.Result = "Error " + "Oasis ID: " + this.OasisAttorneyId + " | CRM message: " + xResp.message;
                    this.ResultID = 2;
                }

            }
        }

        public void UpdateAttorneyInCRM()
        {
            if (this.ID != "")
            {
                var json = JsonConvert.SerializeObject(this, Formatting.None);
                var jsonI = JsonConvert.SerializeObject(this, Formatting.Indented);

                SF_Login();

                var client = new HttpClient();

                string restRequest = this.InstanceUrl + "/services/apexrest/Preset/updateCTXAttorney";
                var request = new HttpRequestMessage(HttpMethod.Post, restRequest);

                request.Headers.Add("Authorization", "Bearer " + this.AuthToken);
                request.Headers.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //request.Headers.Add("X-PrettyPrint", "1");

                var content = new System.Net.Http.StringContent(json, Encoding.UTF8, "application/json");
                request.Content = content;
                var response = client.SendAsync(request).Result;

                string xStr = response.Content.ReadAsStringAsync().Result;
                LogMessage(xStr);

                Attorney_Update_Response xResp = JsonConvert.DeserializeObject<Attorney_Update_Response>(xStr);

                if (xResp.status.ToUpper() == "OK")
                {
                    this.Result = "Success";
                    this.ResultID = 1;
                }
                else
                {
                    this.Result = "Error " + "Oasis ID: " + this.OasisAttorneyId + " | CRM message: " + xResp.message;
                    this.ResultID = 2;
                }
            }

        }

        public void SF_Login()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls; // comparable to modern browsers

            string sfdcConsumerKey = ConfigurationManager.AppSettings["consumerkey"];
            string sfdcConsumerSecret = ConfigurationManager.AppSettings["consumersecret"];
            string sfdcUserName = ConfigurationManager.AppSettings["username"];
            string sfdcPassword = ConfigurationManager.AppSettings["password"];
            string sfdcToken = ConfigurationManager.AppSettings["initialtoken"];

            string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];
            string loginPassword = sfdcPassword + sfdcToken;


            var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                ? ConfigurationManager.AppSettings["SF_URL_test"]
                : ConfigurationManager.AppSettings["SF_URL_prod"];

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "POST";
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";

            StringBuilder body = new StringBuilder();
            body.Append("grant_type=password");
            body.Append("&client_id=" + sfdcConsumerKey);
            body.Append("&client_secret=" + sfdcConsumerSecret);
            body.Append("&username=" + sfdcUserName);
            body.Append("&password=" + sfdcPassword);

            // Add parameters to post
            byte[] data = System.Text.Encoding.ASCII.GetBytes(body.ToString());
            httpWebRequest.ContentLength = data.Length;
            System.IO.Stream os = httpWebRequest.GetRequestStream();
            os.Write(data, 0, data.Length);
            os.Close();

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            LogMessage("Response" + httpResponse);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                LogMessage(result.Replace(":{", ":\n{").Replace(",\"", ",\n\""));

                var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(result);
                this.AuthToken = values["access_token"];
                this.InstanceUrl = values["instance_url"];
            }
        }

        public void LogResults()
        {
            //Console.WriteLine("LogResults: " + this.Result);

            LogMessage("LogResults: " + this.Result);

            StoredProcedure proc = new StoredProcedure("CRM_ActionUpdate");
            proc.AddParameter("@CRM_ActionID", this.CRM_ActionID);
            proc.AddParameter("@CRM_ResponseInfo", this.Result);
            proc.AddParameter("@CRM_StatusID", this.ResultID);
            proc.GetData();
        }

        public static void LogMessage(string message)
        {
            string strMessage = DateTime.Now.ToString("yyyyMMdd HH:mm:ss -- ") + message;
            Console.WriteLine(strMessage);

            DateTime dtNow = DateTime.Now;

            string fileName = AppDomain.CurrentDomain.BaseDirectory + "\\" + dtNow.ToString("yyyyMMdd") + ".log";

            using (StreamWriter log = File.AppendText(fileName))
            {
                log.WriteLine(strMessage);
                log.Close();
            }
        }

        public static string CreateExceptionString(Exception e)
        {
            StringBuilder sb = new StringBuilder();
            CreateExceptionString(sb, e, string.Empty);

            return sb.ToString();
        }

        private static void CreateExceptionString(StringBuilder sb, Exception e, string indent)
        {
            if (indent == null)
            {
                indent = string.Empty;
            }
            else if (indent.Length > 0)
            {
                sb.AppendFormat("{0}Inner ", indent);
            }

            sb.AppendFormat("Exception Found:\n{0}Type: {1}", indent, e.GetType().FullName);
            sb.AppendFormat("\n{0}Message: {1}", indent, e.Message);
            sb.AppendFormat("\n{0}Source: {1}", indent, e.Source);
            sb.AppendFormat("\n{0}Stacktrace: {1}", indent, e.StackTrace);

            if (e.InnerException != null)
            {
                sb.Append("\n");
                CreateExceptionString(sb, e.InnerException, indent + "  ");
            }
        }

    }

    public class Attorney_Response
    {
        public string statusCode { get; set; }
        public string status { get; set; }
        public string message { get; set; }
        public int CTX_FirmID { get; set; }
        public int CTX_AttorneyID { get; set; }
        public string CRM_AttorneyID { get; set; }
    }

    public class Attorney_Update_Response
    {
        public string status { get; set; }
        public string message { get; set; }
    }

}
