﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Salesforce.Force;
using Salesforce.Common;
using Salesforce.Common.Models.Json;
using System.Configuration;
using System.Data;
using System.IO;
using Salesforce.Common.Models.Xml;

namespace Oasis.SFCommunication
{
    public class ViewsOperations
    {
        public const int cRowsCount = 100;


        public async Task CreatePSView()
        {
            StoredProcedure proc = new StoredProcedure("CRM_PSViewAttorneyList");
            DataSet dsAttyList = proc.GetData();

            List<string> AttyIDs = new List<string>();

            foreach (DataRow dr in dsAttyList.Tables[0].Rows)
            {
                AttyIDs.Add(dr["AttorneyID"].ToString());
            }

            while (AttyIDs.Count > 0)
            {
                int count = cRowsCount;

                if (AttyIDs.Count < cRowsCount)
                    count = AttyIDs.Count;

                List<string> Filter200AttyIDs = AttyIDs.GetRange(0, count);

                AttyIDs.RemoveRange(0, count);

                try
                {
                    #region Test1
                    //var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                    //var tsk = client.CreateAsync(SObjectTypeName, this);
                    //tsk.Wait();

                    //SuccessResponse resp = tsk.Result;
                    //if (resp.Success)
                    //{
                    //}
                    #endregion Test1

                    #region Test
                    /**/
                    // Make a strongly typed Account list
                    var stAccountsBatch = new SObjectList<PSView>
                    {
                    };

                    foreach (string strID in Filter200AttyIDs)
                    {

                        PSView pObj = new PSView(Convert.ToInt32(strID));
                        if(pObj.CTX_FirmId__c != null && pObj.CTX_FirmId__c != "")
                        {
                            stAccountsBatch.Add(pObj);
                        }
                        

                    }

                    string SObjectTypeName = "PS_View360__c";

                    string consumerKey = ConfigurationManager.AppSettings["consumerkey"];
                    string consumerSecret = ConfigurationManager.AppSettings["consumersecret"];
                    string username = ConfigurationManager.AppSettings["username"];
                    string password = ConfigurationManager.AppSettings["password"];
                    string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];

                    var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                        ? ConfigurationManager.AppSettings["SF_URL_test"]
                        : ConfigurationManager.AppSettings["SF_URL_prod"];

                    //create auth client to retrieve token
                    var auth = new AuthenticationClient();

                    //get back URL and token
                    try
                    {
                        auth.UsernamePasswordAsync(consumerKey, consumerSecret, username, password, url).Wait();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                    var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                    var tsk = client.RunJobAndPollAsync(SObjectTypeName, BulkConstants.OperationType.Insert, new List<SObjectList<PSView>> { stAccountsBatch });
                    tsk.Wait();


                    for (int i = 0; i < tsk.Result.Count; i++)
                    {
                        for (int j = 0; j < tsk.Result[i].Items.Count; j++)
                        {
                            try
                            {
                                LogMessage("J: " + j.ToString());

                                LogMessage("  Grade: " + stAccountsBatch[j].Attorney_Grade__c.ToString());
                                LogMessage("Atty ID: " + stAccountsBatch[j].PresetID__c.ToString());
                                LogMessage(" CRM ID: " + stAccountsBatch[j].Attorney__c.ToString());

                                LogMessage("Success: " + tsk.Result[i].Items[j].Success.ToString());
                                LogMessage("     ID: " + tsk.Result[i].Items[j].Id);
                                if (tsk.Result[i].Items[j].Errors != null)
                                {
                                    LogMessage("  Error: " + tsk.Result[i].Items[j].Errors.Message);
                                    LogMessage(" Status: " + tsk.Result[i].Items[j].Errors.StatusCode);
                                }
                                LogMessage("#####################################");
                            }
                            catch (Exception ex)
                            {
                                string strEx = CreateExceptionString(ex);
                                LogMessage("Process Atty: " + stAccountsBatch[j].PresetID__c.ToString());
                                LogMessage("EXCEPTION: " + strEx);
                                LogMessage("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                            }

                        }
                    }

                    //foreach (var xResult in tsk.Result)
                    //{
                    //    foreach (var yResult in xResult.Items)
                    //    {
                    //        LogMessage("Success: " + yResult.Success.ToString());
                    //        LogMessage("     ID: " + yResult.Id);
                    //        LogMessage("=====================================");
                    //    }
                    //}

                    /**/
                    #endregion
                }
                catch (Exception ex)
                {
                    string strEx = CreateExceptionString(ex);
                    LogMessage("EXCEPTION: " + strEx);
                }

            }

        }

        public async Task CreatePSLastFundedCasesView()
        {
            StoredProcedure proc = new StoredProcedure("CRM_PSLastInitialFundindViewGet");
            DataSet dsAttyList = proc.GetData();
            if (dsAttyList.Tables.Count > 0)
            {
                if (dsAttyList.Tables[0].Rows.Count > 0)
                {
                    int iRows = dsAttyList.Tables[0].Rows.Count;
                    int iRowsStart = 0;

                    while (iRows > 0)
                    {
                        int count = cRowsCount;
                        if (iRows < cRowsCount)
                        {
                            count = iRows;
                        }

                        var stAccountsBatch = new SObjectList<PSLastFundedCases>
                        {
                        };

                        for (int i = 0; i < count; i++)
                        {
                            DataRow dr = dsAttyList.Tables[0].Rows[i + iRowsStart];

                            PSLastFundedCases pObj = new PSLastFundedCases(dr);
                            if (pObj.CTX_FirmId__c != null && pObj.CTX_FirmId__c != "")
                            {
                                stAccountsBatch.Add(pObj);
                            }
                        }

                        #region SalesForce Call
                        string SObjectTypeName = "PSLastFundedCases__c";

                        string consumerKey = ConfigurationManager.AppSettings["consumerkey"];
                        string consumerSecret = ConfigurationManager.AppSettings["consumersecret"];
                        string username = ConfigurationManager.AppSettings["username"];
                        string password = ConfigurationManager.AppSettings["password"];
                        string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];

                        var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                            ? ConfigurationManager.AppSettings["SF_URL_test"]
                            : ConfigurationManager.AppSettings["SF_URL_prod"];

                        //create auth client to retrieve token
                        var auth = new AuthenticationClient();

                        //get back URL and token
                        try
                        {
                            auth.UsernamePasswordAsync(consumerKey, consumerSecret, username, password, url).Wait();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        try {
                            var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                            var tsk = client.RunJobAndPollAsync(SObjectTypeName, BulkConstants.OperationType.Insert, new List<SObjectList<PSLastFundedCases>> { stAccountsBatch });
                            tsk.Wait();


                            for (int i = 0; i < tsk.Result.Count; i++)
                            {
                                for (int j = 0; j < tsk.Result[i].Items.Count; j++)
                                {
                                    try
                                    {
                                        LogMessage("J: " + j.ToString());

                                        LogMessage("Atty ID: " + stAccountsBatch[j].PresetID__c.ToString());
                                        LogMessage(" CRM ID: " + stAccountsBatch[j].Attorney__c.ToString());

                                        LogMessage("Success: " + tsk.Result[i].Items[j].Success.ToString());
                                        LogMessage("     ID: " + tsk.Result[i].Items[j].Id);
                                        if (tsk.Result[i].Items[j].Errors != null)
                                        {
                                            LogMessage("  Error: " + tsk.Result[i].Items[j].Errors.Message);
                                            LogMessage(" Status: " + tsk.Result[i].Items[j].Errors.StatusCode);
                                        }
                                        LogMessage("#####################################");
                                    }
                                    catch (Exception ex)
                                    {
                                        string strEx = CreateExceptionString(ex);
                                        LogMessage("Process Atty: " + stAccountsBatch[j].PresetID__c.ToString());
                                        LogMessage("EXCEPTION: " + strEx);
                                        LogMessage("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogMessage("======================================================");
                            string strEx = CreateExceptionString(ex);
                            LogMessage("EXCEPTION: " + strEx);
                        }

                        #endregion SalesForce Call

                        iRowsStart = iRowsStart + count;
                        iRows = iRows - count;


                    }
                }
            }
        }

        public async Task CreatePSApprovalQueueView()
        {
            StoredProcedure proc = new StoredProcedure("CRM_PSApprovalQueueViewGet");
            DataSet dsAttyList = proc.GetData();
            if (dsAttyList.Tables.Count > 0)
            {
                if (dsAttyList.Tables[0].Rows.Count > 0)
                {
                    int iRows = dsAttyList.Tables[0].Rows.Count;
                    int iRowsStart = 0;

                    while (iRows > 0)
                    {
                        int count = cRowsCount;
                        if (iRows < cRowsCount)
                        {
                            count = iRows;
                        }

                        var stAccountsBatch = new SObjectList<PSApprovalQueue>
                        {
                        };

                        for (int i = 0; i < count; i++)
                        {
                            DataRow dr = dsAttyList.Tables[0].Rows[i + iRowsStart];

                            PSApprovalQueue pObj = new PSApprovalQueue(dr);
                            if (pObj.CTX_FirmId__c != null && pObj.CTX_FirmId__c != "")
                            {
                                stAccountsBatch.Add(pObj);
                            }
                        }

                        #region SalesForce Call
                        string SObjectTypeName = "PSApprovalQueue__c";

                        string consumerKey = ConfigurationManager.AppSettings["consumerkey"];
                        string consumerSecret = ConfigurationManager.AppSettings["consumersecret"];
                        string username = ConfigurationManager.AppSettings["username"];
                        string password = ConfigurationManager.AppSettings["password"];
                        string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];

                        var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                            ? ConfigurationManager.AppSettings["SF_URL_test"]
                            : ConfigurationManager.AppSettings["SF_URL_prod"];

                        //create auth client to retrieve token
                        var auth = new AuthenticationClient();

                        //get back URL and token
                        try
                        {
                            auth.UsernamePasswordAsync(consumerKey, consumerSecret, username, password, url).Wait();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        try
                        {
                            var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                            var tsk = client.RunJobAndPollAsync(SObjectTypeName, BulkConstants.OperationType.Insert, new List<SObjectList<PSApprovalQueue>> { stAccountsBatch });
                            tsk.Wait();


                            for (int i = 0; i < tsk.Result.Count; i++)
                            {
                                for (int j = 0; j < tsk.Result[i].Items.Count; j++)
                                {
                                    try
                                    {
                                        LogMessage("J: " + j.ToString());

                                        LogMessage("Atty ID: " + stAccountsBatch[j].PresetID__c.ToString());

                                        LogMessage("Success: " + tsk.Result[i].Items[j].Success.ToString());
                                        LogMessage("     ID: " + tsk.Result[i].Items[j].Id);
                                        if (tsk.Result[i].Items[j].Errors != null)
                                        {
                                            LogMessage("  Error: " + tsk.Result[i].Items[j].Errors.Message);
                                            LogMessage(" Status: " + tsk.Result[i].Items[j].Errors.StatusCode);
                                        }
                                        LogMessage("#####################################");
                                    }
                                    catch (Exception ex)
                                    {
                                        string strEx = CreateExceptionString(ex);
                                        LogMessage("Process Atty: " + stAccountsBatch[j].PresetID__c.ToString());
                                        LogMessage("EXCEPTION: " + strEx);
                                        LogMessage("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogMessage("======================================================");
                            string strEx = CreateExceptionString(ex);
                            LogMessage("EXCEPTION: " + strEx);
                        }

                        #endregion SalesForce Call

                        iRowsStart = iRowsStart + count;
                        iRows = iRows - count;


                    }
                }
            }
        }

        public async Task CreatePSPaidInFullView()
        {
            StoredProcedure proc = new StoredProcedure("CRM_PSPaidInFullViewGet");
            DataSet dsAttyList = proc.GetData();
            if (dsAttyList.Tables.Count > 0)
            {
                if (dsAttyList.Tables[0].Rows.Count > 0)
                {
                    int iRows = dsAttyList.Tables[0].Rows.Count;
                    int iRowsStart = 0;

                    while (iRows > 0)
                    {
                        int count = cRowsCount;
                        if (iRows < cRowsCount)
                        {
                            count = iRows;
                        }

                        var stAccountsBatch = new SObjectList<PSPaidInFullView>
                        {
                        };

                        for (int i = 0; i < count; i++)
                        {
                            DataRow dr = dsAttyList.Tables[0].Rows[i + iRowsStart];

                            PSPaidInFullView pObj = new PSPaidInFullView(dr);
                            if (pObj.CTX_FirmId__c != null && pObj.CTX_FirmId__c != "")
                            {
                                stAccountsBatch.Add(pObj);
                            }
                        }

                        #region SalesForce Call
                        string SObjectTypeName = "PS_Last3Cases_Paid_In_Full__c";

                        string consumerKey = ConfigurationManager.AppSettings["consumerkey"];
                        string consumerSecret = ConfigurationManager.AppSettings["consumersecret"];
                        string username = ConfigurationManager.AppSettings["username"];
                        string password = ConfigurationManager.AppSettings["password"];
                        string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];

                        var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                            ? ConfigurationManager.AppSettings["SF_URL_test"]
                            : ConfigurationManager.AppSettings["SF_URL_prod"];

                        //create auth client to retrieve token
                        var auth = new AuthenticationClient();

                        //get back URL and token
                        try
                        {
                            auth.UsernamePasswordAsync(consumerKey, consumerSecret, username, password, url).Wait();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        try
                        {
                            var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                            var tsk = client.RunJobAndPollAsync(SObjectTypeName, BulkConstants.OperationType.Insert, new List<SObjectList<PSPaidInFullView>> { stAccountsBatch });
                            tsk.Wait();


                            for (int i = 0; i < tsk.Result.Count; i++)
                            {
                                for (int j = 0; j < tsk.Result[i].Items.Count; j++)
                                {
                                    try
                                    {
                                        LogMessage("J: " + j.ToString());

                                        LogMessage("Atty ID: " + stAccountsBatch[j].PresetID__c.ToString());

                                        LogMessage("Success: " + tsk.Result[i].Items[j].Success.ToString());
                                        LogMessage("     ID: " + tsk.Result[i].Items[j].Id);
                                        if (tsk.Result[i].Items[j].Errors != null)
                                        {
                                            LogMessage("  Error: " + tsk.Result[i].Items[j].Errors.Message);
                                            LogMessage(" Status: " + tsk.Result[i].Items[j].Errors.StatusCode);
                                        }
                                        LogMessage("#####################################");
                                    }
                                    catch (Exception ex)
                                    {
                                        string strEx = CreateExceptionString(ex);
                                        LogMessage("Process Atty: " + stAccountsBatch[j].PresetID__c.ToString());
                                        LogMessage("EXCEPTION: " + strEx);
                                        LogMessage("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogMessage("======================================================");
                            string strEx = CreateExceptionString(ex);
                            LogMessage("EXCEPTION: " + strEx);
                        }

                        #endregion SalesForce Call

                        iRowsStart = iRowsStart + count;
                        iRows = iRows - count;


                    }
                }
            }
        }

        public async Task CreatePSCaseForServicingView()
        {
            StoredProcedure proc = new StoredProcedure("CRM_PSCaseForServicingViewGet");
            DataSet dsAttyList = proc.GetData();
            if (dsAttyList.Tables.Count > 0)
            {
                if (dsAttyList.Tables[0].Rows.Count > 0)
                {
                    int iRows = dsAttyList.Tables[0].Rows.Count;
                    int iRowsStart = 0;

                    while (iRows > 0)
                    {
                        int count = cRowsCount;
                        if (iRows < cRowsCount)
                        {
                            count = iRows;
                        }

                        var stAccountsBatch = new SObjectList<PSCaseForServicingView>
                        {
                        };

                        for (int i = 0; i < count; i++)
                        {
                            DataRow dr = dsAttyList.Tables[0].Rows[i + iRowsStart];

                            PSCaseForServicingView pObj = new PSCaseForServicingView(dr);
                            if (pObj.CTX_FirmId__c != null && pObj.CTX_FirmId__c != "")
                            {
                                stAccountsBatch.Add(pObj);
                            }
                        }

                        #region SalesForce Call
                        string SObjectTypeName = "PSCasesNeededForServicing__c";

                        string consumerKey = ConfigurationManager.AppSettings["consumerkey"];
                        string consumerSecret = ConfigurationManager.AppSettings["consumersecret"];
                        string username = ConfigurationManager.AppSettings["username"];
                        string password = ConfigurationManager.AppSettings["password"];
                        string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];

                        var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                            ? ConfigurationManager.AppSettings["SF_URL_test"]
                            : ConfigurationManager.AppSettings["SF_URL_prod"];

                        //create auth client to retrieve token
                        var auth = new AuthenticationClient();

                        //get back URL and token
                        try
                        {
                            auth.UsernamePasswordAsync(consumerKey, consumerSecret, username, password, url).Wait();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        try
                        {
                            var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                            var tsk = client.RunJobAndPollAsync(SObjectTypeName, BulkConstants.OperationType.Insert, new List<SObjectList<PSCaseForServicingView>> { stAccountsBatch });
                            tsk.Wait();


                            for (int i = 0; i < tsk.Result.Count; i++)
                            {
                                for (int j = 0; j < tsk.Result[i].Items.Count; j++)
                                {
                                    try
                                    {
                                        LogMessage("J: " + j.ToString());

                                        LogMessage("Atty ID: " + stAccountsBatch[j].PresetID__c.ToString());

                                        LogMessage("Success: " + tsk.Result[i].Items[j].Success.ToString());
                                        LogMessage("     ID: " + tsk.Result[i].Items[j].Id);
                                        if (tsk.Result[i].Items[j].Errors != null)
                                        {
                                            LogMessage("  Error: " + tsk.Result[i].Items[j].Errors.Message);
                                            LogMessage(" Status: " + tsk.Result[i].Items[j].Errors.StatusCode);
                                        }
                                        LogMessage("#####################################");
                                    }
                                    catch (Exception ex)
                                    {
                                        string strEx = CreateExceptionString(ex);
                                        LogMessage("Process Atty: " + stAccountsBatch[j].PresetID__c.ToString());
                                        LogMessage("EXCEPTION: " + strEx);
                                        LogMessage("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogMessage("======================================================");
                            string strEx = CreateExceptionString(ex);
                            LogMessage("EXCEPTION: " + strEx);
                        }

                        #endregion SalesForce Call

                        iRowsStart = iRowsStart + count;
                        iRows = iRows - count;


                    }
                }
            }
        }

        public async Task CreatePSCaseTypeBreakdownView()
        {
            StoredProcedure proc = new StoredProcedure("CRM_PSCaseTypeBreakdownViewGet");
            DataSet dsAttyList = proc.GetData();
            if (dsAttyList.Tables.Count > 0)
            {
                if (dsAttyList.Tables[0].Rows.Count > 0)
                {
                    int iRows = dsAttyList.Tables[0].Rows.Count;
                    int iRowsStart = 0;

                    while (iRows > 0)
                    {
                        int count = cRowsCount;
                        if (iRows < cRowsCount)
                        {
                            count = iRows;
                        }

                        var stAccountsBatch = new SObjectList<PSCaseTypeBreakdownView>
                        {
                        };

                        for (int i = 0; i < count; i++)
                        {
                            DataRow dr = dsAttyList.Tables[0].Rows[i + iRowsStart];

                            PSCaseTypeBreakdownView pObj = new PSCaseTypeBreakdownView(dr);
                            if (pObj.CTX_FirmId__c != null && pObj.CTX_FirmId__c != "")
                            {
                                stAccountsBatch.Add(pObj);
                            }
                        }

                        #region SalesForce Call
                        string SObjectTypeName = "PS_Case_Type_Breakdown__c";

                        string consumerKey = ConfigurationManager.AppSettings["consumerkey"];
                        string consumerSecret = ConfigurationManager.AppSettings["consumersecret"];
                        string username = ConfigurationManager.AppSettings["username"];
                        string password = ConfigurationManager.AppSettings["password"];
                        string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];

                        var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                            ? ConfigurationManager.AppSettings["SF_URL_test"]
                            : ConfigurationManager.AppSettings["SF_URL_prod"];

                        //create auth client to retrieve token
                        var auth = new AuthenticationClient();

                        //get back URL and token
                        try
                        {
                            auth.UsernamePasswordAsync(consumerKey, consumerSecret, username, password, url).Wait();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        try
                        {
                            var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                            var tsk = client.RunJobAndPollAsync(SObjectTypeName, BulkConstants.OperationType.Insert, new List<SObjectList<PSCaseTypeBreakdownView>> { stAccountsBatch });
                            tsk.Wait();


                            for (int i = 0; i < tsk.Result.Count; i++)
                            {
                                for (int j = 0; j < tsk.Result[i].Items.Count; j++)
                                {
                                    try
                                    {
                                        LogMessage("J: " + j.ToString());

                                        LogMessage("Atty ID: " + stAccountsBatch[j].PresetID__c.ToString());

                                        LogMessage("Success: " + tsk.Result[i].Items[j].Success.ToString());
                                        LogMessage("     ID: " + tsk.Result[i].Items[j].Id);
                                        if (tsk.Result[i].Items[j].Errors != null)
                                        {
                                            LogMessage("  Error: " + tsk.Result[i].Items[j].Errors.Message);
                                            LogMessage(" Status: " + tsk.Result[i].Items[j].Errors.StatusCode);
                                        }
                                        LogMessage("#####################################");
                                    }
                                    catch (Exception ex)
                                    {
                                        string strEx = CreateExceptionString(ex);
                                        LogMessage("Process Atty: " + stAccountsBatch[j].PresetID__c.ToString());
                                        LogMessage("EXCEPTION: " + strEx);
                                        LogMessage("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogMessage("======================================================");
                            string strEx = CreateExceptionString(ex);
                            LogMessage("EXCEPTION: " + strEx);
                        }

                        #endregion SalesForce Call

                        iRowsStart = iRowsStart + count;
                        iRows = iRows - count;


                    }
                }
            }
        }

        public async Task CreatePSCaseSourceBreakdownView()
        {
            StoredProcedure proc = new StoredProcedure("CRM_PSCaseSourceBreakdownViewGet");
            DataSet dsAttyList = proc.GetData();
            if (dsAttyList.Tables.Count > 0)
            {
                if (dsAttyList.Tables[0].Rows.Count > 0)
                {
                    int iRows = dsAttyList.Tables[0].Rows.Count;
                    int iRowsStart = 0;

                    while (iRows > 0)
                    {
                        int count = cRowsCount;
                        if (iRows < cRowsCount)
                        {
                            count = iRows;
                        }

                        var stAccountsBatch = new SObjectList<PSCaseSourceBreakdownView>
                        {
                        };

                        for (int i = 0; i < count; i++)
                        {
                            DataRow dr = dsAttyList.Tables[0].Rows[i + iRowsStart];

                            PSCaseSourceBreakdownView pObj = new PSCaseSourceBreakdownView(dr);
                            if (pObj.CTX_FirmId__c != null && pObj.CTX_FirmId__c != "")
                            {
                                stAccountsBatch.Add(pObj);
                            }
                        }

                        #region SalesForce Call
                        string SObjectTypeName = "PS_Case_Source_Breakdown__c";

                        string consumerKey = ConfigurationManager.AppSettings["consumerkey"];
                        string consumerSecret = ConfigurationManager.AppSettings["consumersecret"];
                        string username = ConfigurationManager.AppSettings["username"];
                        string password = ConfigurationManager.AppSettings["password"];
                        string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];

                        var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                            ? ConfigurationManager.AppSettings["SF_URL_test"]
                            : ConfigurationManager.AppSettings["SF_URL_prod"];

                        //create auth client to retrieve token
                        var auth = new AuthenticationClient();

                        //get back URL and token
                        try
                        {
                            auth.UsernamePasswordAsync(consumerKey, consumerSecret, username, password, url).Wait();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        try
                        {
                            var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                            var tsk = client.RunJobAndPollAsync(SObjectTypeName, BulkConstants.OperationType.Insert, new List<SObjectList<PSCaseSourceBreakdownView>> { stAccountsBatch });
                            tsk.Wait();


                            for (int i = 0; i < tsk.Result.Count; i++)
                            {
                                for (int j = 0; j < tsk.Result[i].Items.Count; j++)
                                {
                                    try
                                    {
                                        LogMessage("J: " + j.ToString());

                                        LogMessage("Atty ID: " + stAccountsBatch[j].PresetID__c.ToString());

                                        LogMessage("Success: " + tsk.Result[i].Items[j].Success.ToString());
                                        LogMessage("     ID: " + tsk.Result[i].Items[j].Id);
                                        if (tsk.Result[i].Items[j].Errors != null)
                                        {
                                            LogMessage("  Error: " + tsk.Result[i].Items[j].Errors.Message);
                                            LogMessage(" Status: " + tsk.Result[i].Items[j].Errors.StatusCode);
                                        }
                                        LogMessage("#####################################");
                                    }
                                    catch (Exception ex)
                                    {
                                        string strEx = CreateExceptionString(ex);
                                        LogMessage("Process Atty: " + stAccountsBatch[j].PresetID__c.ToString());
                                        LogMessage("EXCEPTION: " + strEx);
                                        LogMessage("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogMessage("======================================================");
                            string strEx = CreateExceptionString(ex);
                            LogMessage("EXCEPTION: " + strEx);
                        }

                        #endregion SalesForce Call

                        iRowsStart = iRowsStart + count;
                        iRows = iRows - count;


                    }
                }
            }
        }

        public async Task CreatePSPricingBreakdownView()
        {
            StoredProcedure proc = new StoredProcedure("CRM_PSPricingBreakdownViewGet");
            DataSet dsAttyList = proc.GetData();
            if (dsAttyList.Tables.Count > 0)
            {
                if (dsAttyList.Tables[0].Rows.Count > 0)
                {
                    int iRows = dsAttyList.Tables[0].Rows.Count;
                    int iRowsStart = 0;

                    while (iRows > 0)
                    {
                        int count = cRowsCount;
                        if (iRows < cRowsCount)
                        {
                            count = iRows;
                        }

                        var stAccountsBatch = new SObjectList<PSPricingBreakdownView>
                        {
                        };

                        for (int i = 0; i < count; i++)
                        {
                            DataRow dr = dsAttyList.Tables[0].Rows[i + iRowsStart];

                            PSPricingBreakdownView pObj = new PSPricingBreakdownView(dr);
                            if (pObj.CTX_FirmId__c != null && pObj.CTX_FirmId__c != "")
                            {
                                stAccountsBatch.Add(pObj);
                            }
                        }

                        #region SalesForce Call
                        string SObjectTypeName = "PS_Pricing_Breakdown__c";

                        string consumerKey = ConfigurationManager.AppSettings["consumerkey"];
                        string consumerSecret = ConfigurationManager.AppSettings["consumersecret"];
                        string username = ConfigurationManager.AppSettings["username"];
                        string password = ConfigurationManager.AppSettings["password"];
                        string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];

                        var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                            ? ConfigurationManager.AppSettings["SF_URL_test"]
                            : ConfigurationManager.AppSettings["SF_URL_prod"];

                        //create auth client to retrieve token
                        var auth = new AuthenticationClient();

                        //get back URL and token
                        try
                        {
                            auth.UsernamePasswordAsync(consumerKey, consumerSecret, username, password, url).Wait();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        try
                        {
                            var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                            var tsk = client.RunJobAndPollAsync(SObjectTypeName, BulkConstants.OperationType.Insert, new List<SObjectList<PSPricingBreakdownView>> { stAccountsBatch });
                            tsk.Wait();


                            for (int i = 0; i < tsk.Result.Count; i++)
                            {
                                for (int j = 0; j < tsk.Result[i].Items.Count; j++)
                                {
                                    try
                                    {
                                        LogMessage("J: " + j.ToString());

                                        LogMessage("Atty ID: " + stAccountsBatch[j].PresetID__c.ToString());

                                        LogMessage("Success: " + tsk.Result[i].Items[j].Success.ToString());
                                        LogMessage("     ID: " + tsk.Result[i].Items[j].Id);
                                        if (tsk.Result[i].Items[j].Errors != null)
                                        {
                                            LogMessage("  Error: " + tsk.Result[i].Items[j].Errors.Message);
                                            LogMessage(" Status: " + tsk.Result[i].Items[j].Errors.StatusCode);
                                        }
                                        LogMessage("#####################################");
                                    }
                                    catch (Exception ex)
                                    {
                                        string strEx = CreateExceptionString(ex);
                                        LogMessage("Process Atty: " + stAccountsBatch[j].PresetID__c.ToString());
                                        LogMessage("EXCEPTION: " + strEx);
                                        LogMessage("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogMessage("======================================================");
                            string strEx = CreateExceptionString(ex);
                            LogMessage("EXCEPTION: " + strEx);
                        }

                        #endregion SalesForce Call

                        iRowsStart = iRowsStart + count;
                        iRows = iRows - count;


                    }
                }
            }
        }

        public async Task CreatePSUserNotificationsView()
        {
            StoredProcedure proc = new StoredProcedure("CRM_PSUserNotificationsViewGet");
            DataSet dsAttyList = proc.GetData();
            if (dsAttyList.Tables.Count > 0)
            {
                if (dsAttyList.Tables[0].Rows.Count > 0)
                {
                    int iRows = dsAttyList.Tables[0].Rows.Count;
                    int iRowsStart = 0;

                    while (iRows > 0)
                    {
                        int count = cRowsCount;
                        if (iRows < cRowsCount)
                        {
                            count = iRows;
                        }

                        var stAccountsBatch = new SObjectList<PSUserNotificationsView>
                        {
                        };

                        for (int i = 0; i < count; i++)
                        {
                            DataRow dr = dsAttyList.Tables[0].Rows[i + iRowsStart];

                            PSUserNotificationsView pObj = new PSUserNotificationsView(dr);
                            if (pObj.CTX_FirmId__c != null && pObj.CTX_FirmId__c != "")
                            {
                                stAccountsBatch.Add(pObj);
                            }
                        }

                        #region SalesForce Call
                        string SObjectTypeName = "PSUserNotifications__c";

                        string consumerKey = ConfigurationManager.AppSettings["consumerkey"];
                        string consumerSecret = ConfigurationManager.AppSettings["consumersecret"];
                        string username = ConfigurationManager.AppSettings["username"];
                        string password = ConfigurationManager.AppSettings["password"];
                        string IsSandboxUser = ConfigurationManager.AppSettings["IsSandboxUser"];

                        var url = IsSandboxUser.Equals("true", StringComparison.CurrentCultureIgnoreCase)
                            ? ConfigurationManager.AppSettings["SF_URL_test"]
                            : ConfigurationManager.AppSettings["SF_URL_prod"];

                        //create auth client to retrieve token
                        var auth = new AuthenticationClient();

                        //get back URL and token
                        try
                        {
                            auth.UsernamePasswordAsync(consumerKey, consumerSecret, username, password, url).Wait();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }

                        try
                        {
                            var client = new ForceClient(auth.InstanceUrl, auth.AccessToken, auth.ApiVersion);
                            var tsk = client.RunJobAndPollAsync(SObjectTypeName, BulkConstants.OperationType.Insert, new List<SObjectList<PSUserNotificationsView>> { stAccountsBatch });
                            tsk.Wait();


                            for (int i = 0; i < tsk.Result.Count; i++)
                            {
                                for (int j = 0; j < tsk.Result[i].Items.Count; j++)
                                {
                                    try
                                    {
                                        LogMessage("J: " + j.ToString());

                                        LogMessage("Atty ID: " + stAccountsBatch[j].PresetID__c.ToString());

                                        LogMessage("Success: " + tsk.Result[i].Items[j].Success.ToString());
                                        LogMessage("     ID: " + tsk.Result[i].Items[j].Id);
                                        if (tsk.Result[i].Items[j].Errors != null)
                                        {
                                            LogMessage("  Error: " + tsk.Result[i].Items[j].Errors.Message);
                                            LogMessage(" Status: " + tsk.Result[i].Items[j].Errors.StatusCode);
                                        }
                                        LogMessage("#####################################");
                                    }
                                    catch (Exception ex)
                                    {
                                        string strEx = CreateExceptionString(ex);
                                        LogMessage("Process Atty: " + stAccountsBatch[j].PresetID__c.ToString());
                                        LogMessage("EXCEPTION: " + strEx);
                                        LogMessage("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                                    }

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogMessage("======================================================");
                            string strEx = CreateExceptionString(ex);
                            LogMessage("EXCEPTION: " + strEx);
                        }

                        #endregion SalesForce Call

                        iRowsStart = iRowsStart + count;
                        iRows = iRows - count;


                    }
                }
            }
        }

        public static void LogMessage(string message)
        {
            string strMessage = DateTime.Now.ToString("yyyyMMdd HH:mm:ss -- ") + message;
            Console.WriteLine(strMessage);

            DateTime dtNow = DateTime.Now;

            string fileName = AppDomain.CurrentDomain.BaseDirectory + "\\" + dtNow.ToString("yyyyMMdd") + ".log";

            using (StreamWriter log = File.AppendText(fileName))
            {
                log.WriteLine(strMessage);
                log.Close();
            }
        }

        public static string CreateExceptionString(Exception e)
        {
            StringBuilder sb = new StringBuilder();
            CreateExceptionString(sb, e, string.Empty);

            return sb.ToString();
        }

        private static void CreateExceptionString(StringBuilder sb, Exception e, string indent)
        {
            if (indent == null)
            {
                indent = string.Empty;
            }
            else if (indent.Length > 0)
            {
                sb.AppendFormat("{0}Inner ", indent);
            }

            sb.AppendFormat("Exception Found:\n{0}Type: {1}", indent, e.GetType().FullName);
            sb.AppendFormat("\n{0}Message: {1}", indent, e.Message);
            //sb.AppendFormat("\n{0}Source: {1}", indent, e.Source);
            //sb.AppendFormat("\n{0}Stacktrace: {1}", indent, e.StackTrace);

            if (e.InnerException != null)
            {
                sb.Append("\n");
                CreateExceptionString(sb, e.InnerException, indent + "  ");
            }
        }
    }
}
