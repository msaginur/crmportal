﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Salesforce.Force;
using Salesforce.Common;
using Salesforce.Common.Models.Json;
using System.Configuration;
using System.Data;
using System.IO;
using Salesforce.Common.Models.Xml;

namespace Oasis.SFCommunication
{
    public class PSView
    {
        public const string SObjectTypeName = "PS_View360__c";

        public double No_of_Funded_Cases__c { get; set; } //
        public double No_of_Referral_Cases__c { get; set; } //
        public double No_of_Referral_Funded_Cases__c { get; set; } //
        public double No_of_Total_Cases__c { get; set; } //
        public string Account__c { get; set; } //
        public string Account_Relation__c { get; set; } //
        public string Attorney__c { get; set; } //
        public string Attorney_Grade__c { get; set; } //
        public string PSBlackListedTempFundingHold__c { get; set; } //
        public string PSBookofBusiness__c { get; set; } //
        public string PSContractMultiple__c { get; set; } //
        public string CreatedById { get; set; } //
        public string CTX_FirmId__c { get; set; } //
        public string PSDefaultPricing__c { get; set; } //
        public double PSEstimatedProfit__c { get; set; } //
        public double Funded__c { get; set; } //
        public double PSLifetimeRevenue__c { get; set; } //
        public double PSMultipleRatio__c { get; set; } //
        public string OwnerId { get; set; } //
        public string PresetID__c { get; set; } //
        public string PSRealizedMultiple__c { get; set; } //
        public double Referral_Funded__c { get; set; } //
        public string PSServicingRepresentative__c { get; set; } //

        public PSView()
        {
            No_of_Funded_Cases__c = 0;
            No_of_Referral_Cases__c = 0;
            No_of_Referral_Funded_Cases__c = 0;
            No_of_Total_Cases__c = 0;
            Account__c = "";
            Account_Relation__c = "";
            Attorney__c = "";
            Attorney_Grade__c = "";
            PSBlackListedTempFundingHold__c = "";
            PSBookofBusiness__c = "";
            PSContractMultiple__c = "";
            CTX_FirmId__c = "";
            PSDefaultPricing__c = "";
            PSEstimatedProfit__c = 0;
            Funded__c = 0;
            PSLifetimeRevenue__c = 0;
            PSMultipleRatio__c = 0;
            PresetID__c = "";
            PSRealizedMultiple__c = "";
            Referral_Funded__c = 0;
            PSServicingRepresentative__c = "";
        }

        public PSView(int iAttorneyID)
        {
            StoredProcedure proc = new StoredProcedure("CRM_PSViewGet");
            proc.AddParameter("@attorneyID", iAttorneyID);
            DataSet ds = proc.GetData();
            //string //strField = "";

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        Attorney_Grade__c = (ds.Tables[0].Rows[0]["Grade"].ToString() == null ? "" : ds.Tables[0].Rows[0]["Grade"].ToString());
                        PSBookofBusiness__c = ds.Tables[0].Rows[0]["BookOfBusinessOwner"].ToString();
                        CTX_FirmId__c = ds.Tables[0].Rows[0]["PS_FirmId"].ToString();
                        Account__c = ds.Tables[0].Rows[0]["CRM_FirmId"].ToString();
                        Attorney__c = ds.Tables[0].Rows[0]["CRM_AttorneyId"].ToString();
                        PSServicingRepresentative__c = ds.Tables[0].Rows[0]["ServicingRepresentative"].ToString();
                        PSRealizedMultiple__c = ds.Tables[0].Rows[0]["RealizedMultiple"].ToString();
                        PSContractMultiple__c = ds.Tables[0].Rows[0]["ContractMultiple"].ToString();
                        PSMultipleRatio__c = (ds.Tables[0].Rows[0]["MultipleRatio"] == System.DBNull.Value ? 0 : Convert.ToDouble(ds.Tables[0].Rows[0]["MultipleRatio"]));
                        PresetID__c = ds.Tables[0].Rows[0]["PS_AttorneyId"].ToString();
                        PSBlackListedTempFundingHold__c = ds.Tables[0].Rows[0]["FundingHold"].ToString();
                        PSDefaultPricing__c = ds.Tables[0].Rows[0]["defaultPricing"].ToString();
                        PSEstimatedProfit__c = Convert.ToDouble(ds.Tables[0].Rows[0]["EstimatedNetProfit"]);
                        Referral_Funded__c = Convert.ToDouble(ds.Tables[0].Rows[0]["totalReferralFundingAmount"]);
                        Funded__c = Convert.ToDouble(ds.Tables[0].Rows[0]["totalFundedAmount"]);
                        No_of_Total_Cases__c = Convert.ToDouble(ds.Tables[0].Rows[0]["TotalCases"]);
                        No_of_Funded_Cases__c = Convert.ToDouble(ds.Tables[0].Rows[0]["totalFundedCount"]);
                        No_of_Referral_Cases__c = Convert.ToDouble(ds.Tables[0].Rows[0]["referralCount"]);
                    }
                    catch (Exception ex)
                    {
                        LogMessage("======================================================");
                        LogMessage("PSView - " + iAttorneyID.ToString());
                        string strEx = CreateExceptionString(ex);
                        LogMessage("EXCEPTION: " + strEx);
                    }
                }
            }
        }

        public static void LogMessage(string message)
        {
            string strMessage = DateTime.Now.ToString("yyyyMMdd HH:mm:ss -- ") + message;
            Console.WriteLine(strMessage);

            DateTime dtNow = DateTime.Now;

            string fileName = AppDomain.CurrentDomain.BaseDirectory + "\\" + dtNow.ToString("yyyyMMdd") + ".log";

            using (StreamWriter log = File.AppendText(fileName))
            {
                log.WriteLine(strMessage);
                log.Close();
            }
        }

        public static string CreateExceptionString(Exception e)
        {
            StringBuilder sb = new StringBuilder();
            CreateExceptionString(sb, e, string.Empty);

            return sb.ToString();
        }

        private static void CreateExceptionString(StringBuilder sb, Exception e, string indent)
        {
            if (indent == null)
            {
                indent = string.Empty;
            }
            else if (indent.Length > 0)
            {
                sb.AppendFormat("{0}Inner ", indent);
            }

            sb.AppendFormat("Exception Found:\n{0}Type: {1}", indent, e.GetType().FullName);
            sb.AppendFormat("\n{0}Message: {1}", indent, e.Message);
            //sb.AppendFormat("\n{0}Source: {1}", indent, e.Source);
            //sb.AppendFormat("\n{0}Stacktrace: {1}", indent, e.StackTrace);

            if (e.InnerException != null)
            {
                sb.Append("\n");
                CreateExceptionString(sb, e.InnerException, indent + "  ");
            }
        }

    }
}
