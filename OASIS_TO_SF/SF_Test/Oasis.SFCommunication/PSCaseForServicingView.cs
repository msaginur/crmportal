﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

namespace Oasis.SFCommunication
{
    public class PSCaseForServicingView
    {

        public const string SObjectTypeName = "PSCasesNeededForServicing__c";
        public string Account__c { get; set; } //
        public string PSAttorney__c { get; set; } //(Contact)
        public string CTX_FirmId__c { get; set; } //Text(255)
        public string PSPlaintiffName__c { get; set; } //Text(255)		False	
        public string PresetID__c { get; set; } //Text(255)
        public string LastNotes__c { get; set; } //
        public double PSFunded__c { get; set; } //Currency(16, 2)
        public double PSOwed__c { get; set; } //Currency(16, 2)


        public PSCaseForServicingView()
        {
        }

        public PSCaseForServicingView(DataRow dr)
        {
            try
            {
                Account__c = dr["CRM_FirmId"].ToString();
                PSAttorney__c = dr["CRM_AttorneyId"].ToString();
                CTX_FirmId__c = dr["PS_FirmId"].ToString();
                PSPlaintiffName__c = dr["PlaintiffName"].ToString();
                PresetID__c = dr["PS_AttorneyId"].ToString();

                LastNotes__c = dr["LastServicingNote"].ToString();
                PSFunded__c = Convert.ToDouble((dr["TotalFunded"] == System.DBNull.Value ? 0 : dr["TotalFunded"]));
                PSOwed__c = Convert.ToDouble((dr["TotalOwed"] == System.DBNull.Value ? 0 : dr["TotalOwed"]));
            }
            catch (Exception ex)
            {
                LogMessage("======================================================");
                string strEx = CreateExceptionString(ex);
                LogMessage("EXCEPTION: " + strEx);
            }
        }



        public static void LogMessage(string message)
        {
            string strMessage = DateTime.Now.ToString("yyyyMMdd HH:mm:ss -- ") + message;
            Console.WriteLine(strMessage);

            DateTime dtNow = DateTime.Now;

            string fileName = AppDomain.CurrentDomain.BaseDirectory + "\\" + dtNow.ToString("yyyyMMdd") + ".log";

            using (StreamWriter log = File.AppendText(fileName))
            {
                log.WriteLine(strMessage);
                log.Close();
            }
        }

        public static string CreateExceptionString(Exception e)
        {
            StringBuilder sb = new StringBuilder();
            CreateExceptionString(sb, e, string.Empty);

            return sb.ToString();
        }

        private static void CreateExceptionString(StringBuilder sb, Exception e, string indent)
        {
            if (indent == null)
            {
                indent = string.Empty;
            }
            else if (indent.Length > 0)
            {
                sb.AppendFormat("{0}Inner ", indent);
            }

            sb.AppendFormat("Exception Found:\n{0}Type: {1}", indent, e.GetType().FullName);
            sb.AppendFormat("\n{0}Message: {1}", indent, e.Message);
            //sb.AppendFormat("\n{0}Source: {1}", indent, e.Source);
            //sb.AppendFormat("\n{0}Stacktrace: {1}", indent, e.StackTrace);

            if (e.InnerException != null)
            {
                sb.Append("\n");
                CreateExceptionString(sb, e.InnerException, indent + "  ");
            }
        }


    }
}
