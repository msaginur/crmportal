﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Oasis.SFCommunication
{
    public class PSLastFundedCases
    {
        public const string SObjectTypeName = "PSLastFundedCases__c";
        public string Account__c { get; set; } //Lookup(Account)		True	
        public string Attorney__c { get; set; } //Lookup(Contact)		True	
        public string PSCaseType__c { get; set; } //Text(255)		False	
        public string CTX_FirmId__c { get; set; } //Text(255)		False	
        public string PSDateFunded__c { get; set; } //Date		False	
        public string PSPlaintiffName__c { get; set; } //Text(255)		False	
        public string PresetID__c { get; set; } //Text(255)		False	
        public string PSPricingCategory__c { get; set; } //Text(255)		False	

        public PSLastFundedCases()
        {
        }

        public PSLastFundedCases(DataRow dr)
        {
            try
            {
                Account__c = dr["CRM_FirmId"].ToString();
                Attorney__c = dr["CRM_AttorneyId"].ToString();
                PSCaseType__c = dr["CaseType"].ToString();
                CTX_FirmId__c = dr["PS_FirmId"].ToString();
                PSPlaintiffName__c = dr["FullName"].ToString();  //Text(255)		False	
                PresetID__c = dr["PS_AttorneyId"].ToString();
                PSPricingCategory__c = dr["PricingCategory"].ToString();
                PSDateFunded__c = Convert.ToDateTime(dr["TransactionDate"]).ToString("yyyy-MM-dd");
            }
            catch (Exception ex)
            {
                LogMessage("======================================================");
                string strEx = CreateExceptionString(ex);
                LogMessage("EXCEPTION: " + strEx);
            }
        }

        public static void LogMessage(string message)
        {
            string strMessage = DateTime.Now.ToString("yyyyMMdd HH:mm:ss -- ") + message;
            Console.WriteLine(strMessage);

            DateTime dtNow = DateTime.Now;

            string fileName = AppDomain.CurrentDomain.BaseDirectory + "\\" + dtNow.ToString("yyyyMMdd") + ".log";

            using (StreamWriter log = File.AppendText(fileName))
            {
                log.WriteLine(strMessage);
                log.Close();
            }
        }

        public static string CreateExceptionString(Exception e)
        {
            StringBuilder sb = new StringBuilder();
            CreateExceptionString(sb, e, string.Empty);

            return sb.ToString();
        }

        private static void CreateExceptionString(StringBuilder sb, Exception e, string indent)
        {
            if (indent == null)
            {
                indent = string.Empty;
            }
            else if (indent.Length > 0)
            {
                sb.AppendFormat("{0}Inner ", indent);
            }

            sb.AppendFormat("Exception Found:\n{0}Type: {1}", indent, e.GetType().FullName);
            sb.AppendFormat("\n{0}Message: {1}", indent, e.Message);
            //sb.AppendFormat("\n{0}Source: {1}", indent, e.Source);
            //sb.AppendFormat("\n{0}Stacktrace: {1}", indent, e.StackTrace);

            if (e.InnerException != null)
            {
                sb.Append("\n");
                CreateExceptionString(sb, e.InnerException, indent + "  ");
            }
        }


    }
}
