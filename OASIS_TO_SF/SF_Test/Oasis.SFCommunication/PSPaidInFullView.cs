﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

namespace Oasis.SFCommunication
{
    public class PSPaidInFullView
    {
        public const string SObjectTypeName = "PS_Last3Cases_Paid_In_Full__c";
        public string Account__c { get; set; } //
        public string Attorney__c { get; set; } //(Contact)
        public string Case_Number__c { get; set; } //
        public double Amount_Paid__c { get; set; } //Currency(16, 2)
        public string CTX_FirmId__c { get; set; } //Text(255)
        public string Last_Payment_Date__c { get; set; } //Text(255)
        public string PSPlaintiffName__c { get; set; } //Text(255)		False	
        public string PresetID__c { get; set; } //Text(255)

        public PSPaidInFullView()
        {
        }

        public PSPaidInFullView(DataRow dr)
        {
            try
            {
                Account__c = dr["CRM_FirmId"].ToString();
                Attorney__c = dr["CRM_AttorneyId"].ToString();
                Case_Number__c = dr["CaseNumber"].ToString();
                Amount_Paid__c = Convert.ToDouble(dr["totalCollected"]);
                CTX_FirmId__c = dr["PS_FirmId"].ToString();
                Last_Payment_Date__c = Convert.ToDateTime(dr["lastPaymentDate"]).ToString("yyyy-MM-dd");
                PSPlaintiffName__c = dr["PlaintiffName"].ToString();
                PresetID__c = dr["PS_AttorneyId"].ToString();
            }
            catch (Exception ex)
            {
                LogMessage("======================================================");
                string strEx = CreateExceptionString(ex);
                LogMessage("EXCEPTION: " + strEx);
            }
        }

        public static void LogMessage(string message)
        {
            string strMessage = DateTime.Now.ToString("yyyyMMdd HH:mm:ss -- ") + message;
            Console.WriteLine(strMessage);

            DateTime dtNow = DateTime.Now;

            string fileName = AppDomain.CurrentDomain.BaseDirectory + "\\" + dtNow.ToString("yyyyMMdd") + ".log";

            using (StreamWriter log = File.AppendText(fileName))
            {
                log.WriteLine(strMessage);
                log.Close();
            }
        }

        public static string CreateExceptionString(Exception e)
        {
            StringBuilder sb = new StringBuilder();
            CreateExceptionString(sb, e, string.Empty);

            return sb.ToString();
        }

        private static void CreateExceptionString(StringBuilder sb, Exception e, string indent)
        {
            if (indent == null)
            {
                indent = string.Empty;
            }
            else if (indent.Length > 0)
            {
                sb.AppendFormat("{0}Inner ", indent);
            }

            sb.AppendFormat("Exception Found:\n{0}Type: {1}", indent, e.GetType().FullName);
            sb.AppendFormat("\n{0}Message: {1}", indent, e.Message);
            //sb.AppendFormat("\n{0}Source: {1}", indent, e.Source);
            //sb.AppendFormat("\n{0}Stacktrace: {1}", indent, e.StackTrace);

            if (e.InnerException != null)
            {
                sb.Append("\n");
                CreateExceptionString(sb, e.InnerException, indent + "  ");
            }
        }

    }
}
