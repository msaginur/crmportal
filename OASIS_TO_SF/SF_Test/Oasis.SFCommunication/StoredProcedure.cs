using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

/// <summary>
/// Simplifies calling a stored procedure and returning data
/// </summary>
public class StoredProcedure
{
    public enum DatabaseName { Default, Oasis, Corporate, RightFax, Voice }     // Voice added #26853 WPO
    public enum AppEnvironment { Default, Production, Test, Development, Reporting }

    private SqlCommand cmd;
    private int intDefaultTimeout = 300;
    private DatabaseName dbSelected;
    private AppEnvironment environment;

    // ----- Initialize the object -----

    public StoredProcedure(string procedureName)
    {
        Initialize(procedureName, DatabaseName.Oasis);
    }

    public StoredProcedure(string procedureName, DatabaseName databaseName)
    {
        Initialize(procedureName, databaseName);
    }

    public StoredProcedure(string procedureName, AppEnvironment environment)
    {
        Initialize(procedureName, DatabaseName.Oasis, environment);
    }

    private void Initialize(string procedureName, DatabaseName databaseName)
    {
        Initialize(procedureName, databaseName, AppEnvironment.Default);
    }

    private void Initialize(string procedureName, DatabaseName databaseName, AppEnvironment environment)
    {
        dbSelected = databaseName;
        this.environment = environment;

        cmd = new SqlCommand(procedureName);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandTimeout = intDefaultTimeout;
    }
    // ----- SqlConnection -----

    public static SqlConnection Connection()
    {
        return Connection(DatabaseName.Default, AppEnvironment.Default);
    }

    public static SqlConnection Connection(DatabaseName databaseName)
    {
        return Connection(databaseName, AppEnvironment.Default);
    }

    public static SqlConnection Connection(AppEnvironment environment)
    {
        return Connection(DatabaseName.Default, environment);
    }

    public static SqlConnection Connection(DatabaseName databaseName, AppEnvironment environment)
    {
        SqlConnection Conn = new SqlConnection(ConnectionString(databaseName, environment));
        return Conn;
    }

    // ----- Connection String -----

    public static string ConnectionString()
    {
        return ConnectionString(DatabaseName.Default, AppEnvironment.Default);
    }

    public static string ConnectionString(DatabaseName databaseName)
    {
        return ConnectionString(databaseName, AppEnvironment.Default);
    }

    public static string ConnectionString(AppEnvironment environment)
    {
        return ConnectionString(DatabaseName.Default, environment);
    }
    
    public static string ConnectionString(DatabaseName databaseName, AppEnvironment environment)
    {
        string strCon = null;

        if (databaseName == DatabaseName.Default)
        {
            databaseName = DatabaseName.Oasis;
        }

        if (environment == AppEnvironment.Default)
        {
            environment = (AppEnvironment) Enum.Parse(typeof(AppEnvironment), ConfigurationManager.AppSettings["environment"].ToString());
        }
        
        switch (databaseName)
        {
            case DatabaseName.Corporate:
                switch (environment)
                {
                    case AppEnvironment.Development:
                        strCon = ConfigurationManager.ConnectionStrings["CorpConnection_Dev"].ConnectionString;
                        break;
                    case AppEnvironment.Test:
                        strCon = ConfigurationManager.ConnectionStrings["CorpConnection_Test"].ConnectionString;
                        break;
                    case AppEnvironment.Production:
                        strCon = ConfigurationManager.ConnectionStrings["CorpConnection_Prod"].ConnectionString;
                        break;
                    case AppEnvironment.Reporting:
                        strCon = ConfigurationManager.ConnectionStrings["CorpConnection_RPT"].ConnectionString;
                        break;
                }
                break;
            case DatabaseName.Oasis:
                switch (environment)
                {
                    case AppEnvironment.Development:
                        strCon = ConfigurationManager.ConnectionStrings["OasisConnection_Dev"].ConnectionString;
                        break;
                    case AppEnvironment.Test:
                        strCon = ConfigurationManager.ConnectionStrings["OasisConnection_Test"].ConnectionString;
                        break;
                    case AppEnvironment.Production:
                        strCon = ConfigurationManager.ConnectionStrings["OasisConnection_Prod"].ConnectionString;
                        break;
                    case AppEnvironment.Reporting:
                        strCon = ConfigurationManager.ConnectionStrings["OasisConnection_RPT"].ConnectionString;
                        break;
                }
                break;
            case DatabaseName.RightFax:
                {
                    strCon = ConfigurationManager.ConnectionStrings["RightFaxConnection_Prod"].ConnectionString;
                }
                break;
            case DatabaseName.Voice:
                {   // WPO #26853
                    strCon = ConfigurationManager.ConnectionStrings["VoiceConnection"].ConnectionString;
                }
                break;
        }

        return strCon;
    }

    // ----- Set the parameters and options on the stored procedure before execution -----

    /// <summary>
    /// Add a parameter to the procedure call
    /// </summary>
    /// <param name="parameterName">Name of the parameter variable</param>
    /// <param name="value">Value to pass to this parameter</param>
    public void AddParameter(string parameterName, object value)
    {
        cmd.Parameters.AddWithValue(parameterName, value);
    }

    /// <summary>
    /// set parameter type for added performance
    /// </summary>
    /// <param name="parameterName"></param>
    /// <param name="value"></param>
    /// <param name="dbType"></param>
    public void AddParameter(string parameterName, object value, SqlDbType dbType)
    {
        SqlParameter param = new SqlParameter(parameterName, dbType);
        param.Value = value;
        cmd.Parameters.Add(param);
    }

    /// <summary>
    /// Add an output parameter to the procedure call
    /// </summary>
    /// <param name="parameterName">Name of the parameter variable</param>
    /// <param name="dbType">Data type of the output value</param>
    public void AddOutputParameter(string parameterName, SqlDbType dbType)
    {
        SqlParameter param = new SqlParameter(parameterName, dbType);
        param.Direction = ParameterDirection.Output;
        cmd.Parameters.Add(param);
    }

    /// <summary>
    /// Add an output parameter to the procedure call
    /// </summary>
    /// <param name="parameterName">Name of the parameter variable</param>
    /// <param name="dbType">Data type of the output value</param>
    /// <param name="value">Value to pass to this parameter</param>
    public void AddOutputParameter(string parameterName, SqlDbType dbType, object value)
    {
        SqlParameter param = new SqlParameter(parameterName, dbType);
        param.Direction = ParameterDirection.InputOutput;
        param.Value = value;
        cmd.Parameters.Add(param);
    }

    /// <summary>
    /// Get the value of an output parameter after running the procedure
    /// </summary>
    /// <param name="parameterName">Name of the parameter variable</param>
    /// <returns>The value of the parameter</returns>
    public object GetOutputValue(string parameterName)
    {
        return cmd.Parameters[parameterName].SqlValue;
    }

    /// <summary>
    /// The wait time before terminating the attempt to execute a command and generating an error (in seconds)
    /// </summary>
    public int Timeout
    {
        get { return cmd.CommandTimeout; }
        set { cmd.CommandTimeout = value; }
    }

    // ----- Execute the Stored Procedure -----

    public DataSet GetData()
    {
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();

        using (SqlConnection con = Connection(dbSelected, environment))
        {
            cmd.Connection = con;
            da.SelectCommand = cmd;

            con.Open();
            da.Fill(ds);
        }
        return ds;
    }

    public string GetXML(string strRootNode)
    {
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet(strRootNode);

        using (SqlConnection con = Connection(dbSelected))
        {
            cmd.Connection = con;
            da.SelectCommand = cmd;

            con.Open();
            da.Fill(ds);
        }
        return ds.GetXml();
    }

    public object GetScalar()
    {
        using (SqlConnection con = Connection(dbSelected))
        {
            cmd.Connection = con;

            con.Open();
            return cmd.ExecuteScalar();
        }
    }

    public void ExecuteNonQuery()
    {
        using (SqlConnection con = Connection(dbSelected))
        {
            cmd.Connection = con;

            con.Open();
            cmd.ExecuteNonQuery();
        }
    }

    public void ExecuteNonQuery(AppEnvironment env)
    {
        using (SqlConnection con = Connection(dbSelected, env))
        {
            cmd.Connection = con;

            con.Open();
            cmd.ExecuteNonQuery();
        }
    }
}
