--INSERT INTO [dbo].[CRM_Action]([CRM_ActionTypeID],[CRM_ActionName],[CRM_OasisID],[CRM_ResponseInfo],[CRM_StatusID],[CRM_DateCreated],[CRM_CreatedBy])
SELECT top 10 1, 'CREATE_FIRM', a.OrganizationID, '', 0, GETDATE(), 426
FROM Organization a
WHERE a.OrganizationType = 221
and a.CRM_ID IS NULL
order by a.OrganizationID


--INSERT INTO [dbo].[CRM_Action]([CRM_ActionTypeID],[CRM_ActionName],[CRM_OasisID],[CRM_ResponseInfo],[CRM_StatusID],[CRM_DateCreated],[CRM_CreatedBy]) 
SELECT 3, 'CREATE_ATTORNEY', b.PersonID, '', 0, GETDATE(), 426
FROM Organization a
join CRM_Action x on x.CRM_OasisID = a.OrganizationID
join PersonOrganization b on a.OrganizationID = b.OrganizationID
join Person d on d.PersonID = b.PersonID
WHERE a.OrganizationType = 221
order by a.OrganizationID, b.PersonID

SELECT *, a.CRM_ResponseInfo 
--UPDATE a SET a.CRM_StatusID = 0, a.CRM_ResponseInfo = ''
FROM CRM_Action a
join PersonOrganization b on b.PersonID = a.CRM_OasisID
and b.OrganizationID = 67
WHERE a.CRM_ActionName = 'CREATE_ATTORNEY'
--and a.CRM_OasisID <> 67


SELECT ad.CRM_ID, a.*
--UPDATE a SET a.CRM_StatusID = 0, a.CRM_ResponseInfo = ''
--UPDATE ad SET ad.CRM_ID = NULL
FROM CRM_Action a
join PersonOrganization b on b.PersonID = a.CRM_OasisID
and b.OrganizationID = 68
join AttorneyDetails ad on ad.AttorneyID = a.CRM_OasisID
WHERE a.CRM_ActionName = 'CREATE_ATTORNEY'
--and a.CRM_OasisID <> 67


SELECT *
--UPDATE a SET a.CRM_StatusID = 0, a.CRM_ResponseInfo = ''
--UPDATE b SET b.CRM_ID = NULL
FROM CRM_Action a
join Organization b on a.CRM_OasisID = b.OrganizationID
WHERE a.CRM_ActionName = 'CREATE_FIRM'
and a.CRM_OasisID = 68

SELECT * FROM CRM_ActionType

SELECT *
--UPDATE a SET a.CRM_StatusID = 0, a.CRM_ResponseInfo = '', a.CRM_ActionTypeID = 2, a.CRM_ActionName = 'UPDATE_FIRM'
--UPDATE b SET b.CRM_ID = NULL
FROM CRM_Action a
join Organization b on a.CRM_OasisID = b.OrganizationID
WHERE a.CRM_ActionName = 'CREATE_FIRM'
and a.CRM_StatusID = 0
and a.CRM_OasisID = 68



SELECT *
--UPDATE a SET a.CRM_StatusID = 0, a.CRM_ResponseInfo = '', a.CRM_ActionTypeID = 2, a.CRM_ActionName = 'UPDATE_FIRM'
--UPDATE b SET b.CRM_ID = NULL
--UPDATE a SET a.CRM_StatusID = 5
FROM CRM_Action a
join Organization b on a.CRM_OasisID = b.OrganizationID
WHERE a.CRM_ActionName = 'UPDATE_FIRM'
and a.CRM_StatusID = 0
and b.CRM_ID IS NOT NULL


SELECT *
--UPDATE a SET a.CRM_StatusID = 5
FROM CRM_Action a
WHERE a.CRM_StatusID = 0


SELECT d.*
FROM CRM_Action a
join Organization b on a.CRM_OasisID = b.OrganizationID
join vPersonOrganization c on c.OrganizationID = b.OrganizationID
join CRM_Action d on d.CRM_OasisID = c.PersonID
WHERE a.CRM_ActionName = 'CREATE_FIRM'
and d.CRM_ActionName = 'CREATE_ATTORNEY'
