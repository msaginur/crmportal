SELECT a.OrganizationID
, a.OrganizationName
, c.OrganizationAddressId
, c.AddressID
, e.Persons
, d.*
FROM Organization a
left outer join OrganizationAddress c on a.OrganizationID = c.OrganizationID
left outer join Address d on d.AddressID = c.AddressID
join (
	SELECT a.OrganizationName, [zz] = count(*)
	FROM Organization a
	GROUP BY a.OrganizationName 
	HAVING count(*) > 1
) b on a.OrganizationName = b.OrganizationName
left outer join (
	SELECT a.OrganizationID, [Persons] = count(*)
	FROM PersonOrganization a
	GROUP BY a.OrganizationID
) e on e.OrganizationID = a.OrganizationID
WHERE a.OrganizationName <> ''
ORDER BY a.OrganizationName, a.OrganizationID

